<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SHORT-APP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- iCheck --> 
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

    <link rel="stylesheet" href="bootstrap/css/custom.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="container">
   
 
    <div class="row" style="padding: 7px;">
        <div class="col-md-12">
           <div class="card">
            <div class="card-header">
              <h2 class="card-title pull-left" id="appheading" style="margin-left: 10px;">Submited Answer</h2>
             
            </div>
   

    <div class="stepwizard" id="stepwizardContainer">
        <div class="stepwizard-row setup-panel">                      
        </div>
    </div>
        <form role="form" id="subForm" >

        </form>
    </div>
    <!-- /.login-card-body -->
  </div></div>
<!-- /.login-box -->





<!-- jQuery -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/iCheck/icheck.min.js"></script>
<script src="bootstrap/js/custom.js"></script> 

<script src="dist/js/pages/wizardValidation2.js"></script>
<script src="plugins/bootstrap-notify-master/bootstrap-notify.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script>

$(document).ready(function(){
  var appid = `<?php echo $_REQUEST['applicationId']; ?>` 
  var newuserid = `<?php echo ($_REQUEST['newuserid']); ?>`
                
  if(appid && newuserid){
    getWizardData(appid, newuserid);
  }

  $.ajax({
          type: "POST",
          url: "config/ajax.php",         
          dataType: "json",
          data: {getfont:'yes',fontappid:appid},        
          success: function(data) { 
            console.log('Data..@@@.....',data[0])
            if(data[0].applicationName){
              $('#appheading').text((data[0].applicationName).toUpperCase())
            }
              
            },
          error: function(e) {
              console.log(e);
          }
      });
 

});

</script>



</body>
</html>
