<?php 
require(__DIR__ . "/../../vendor/autoload.php");
$applicationId = $_REQUEST['applicationId'];
$memberId = $_SESSION['MEMBER_ID'];
 $query = "SELECT questions.*, answers.answer, answers.memberId as memberId, applicationDetails.convertedImages as imagename FROM `answers` LEFT JOIN questions ON answers.questionId = questions.id LEFT JOIN applicationDetails ON questions.imageId = applicationDetails.id WHERE answers.applicationId=$applicationId AND answers.memberId=$memberId";
 
$answer = fetch_custom($query);

$images = fetch_multiple_with_key('applicationDetails','*','applicationId',$applicationId);

foreach($images as $img){
  $data = [];
  foreach($answer as $result){  
        if($result['imagename']== $img['convertedImages']){
            array_push($data,$result);
        }
  } 
  if($data){
 //   header ('Content-Type: image/jpeg');
    $im = imagecreatefromjpeg('assest/convertedImages/'.$img['convertedImages']);
    $fontFamily = 'bootstrap/fonts/OpenSans-Light.ttf';
    $ans = '';
    $fontColor = '0,0,0';
    $fontSize = 12;
    $angel = 0;
  foreach($data as $res){
    $xCordinate = $res['xCordinates'];
    $yCordinate = $res['yCordinates'];

       if($res['questionType']==='checkbox'){
        $fontFamily = 'bootstrap/fonts/OpenSans-Light.ttf';
        $ans = 'x';
        $fontColor = '0,0,0';
        $fontSize = 14; 
       }else{
        $ans = $res['answer'];
         $fontColor = $res['fontColor'];
         $fontFamily = $res['fontFamily']!=''?'bootstrap/fonts/'.$res['fontFamily'].'.ttf' : $fontFamily;
       }
        $color = explode(',',$fontColor);
       $text_color = imagecolorallocate($im, $color[0],$color[1],$color[2]);
       imagettftext($im, $fontSize, $angel, $xCordinate, $yCordinate, $text_color, $fontFamily, $ans);

  }
  imagejpeg($im,"./assest/answers/".$img['convertedImages']);
imagedestroy($im);
}

}
 $finalImages = [];
foreach($images as $img){

  if(file_exists("./assest/answers/".$img['convertedImages'])){
    array_push($finalImages,"./assest/answers/".$img['convertedImages']);
  }else{
    array_push($finalImages,"./assest/convertedImages/".$img['convertedImages']);
  }
}
$pdf = new Imagick($finalImages);
$pdf->setImageFormat('pdf');
$pdfName = rand().'.pdf';
$pdf->writeImages('./assest/answers/'.$pdfName, true);

?>
<script>
var pdfPath = `<?php echo './assest/answers/'.$pdfName; ?>`;
    window.open(pdfPath);
    
var applicationId = `<?php echo  $applicationId; ?>`;
    window.location.href = '?action=applicationList';
</script>
<?php

?>