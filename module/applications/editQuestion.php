  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

  <link rel="stylesheet" media="screen" type="text/css" href="plugins/colorpicker/css/colorpicker.css" />

  <link rel="stylesheet" href="bootstrap/css/custom.css">  
<section class="content">
    <div class="row" style="margin-left: -7px;padding: 7px;">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-left">Edit Applications</h3>
                   
                </div>
                <!-- /.card-header -->
                <div class="card-body pad-around">
                   <!-- //////////////////////////////////////////// -->
                     <table class="table table-hover" id="table_id">
                        <thead>
                            <tr>
                                <th scope="col">Number</th>
                                <th scope="col">Question</th>
                                <th scope="col">Question Type</th>
                                <th scope="col">Mapped with</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody id="edques">
                        </tbody>
                    </table>





                   <!-- //////////////////////////////////////////// -->
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
    </section>

  <div class="modal fade in" id="editModal" role="dialog">
      <div class="modal-dialog" role="document" style="width:97%">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Question</h4>
              </div>

              <div class="modal-body">
                <div class="row">
                  <div class="col-md-9">
                     <div style="width:100%;overflow:scroll">
                        <img src="" id="editImg" style="width:auto" />
                      </div>
                  </div>

                  <div class="col-md-3">
                      <div class="card card-primary" style="padding:8px">
                          <div class="card-header">
                              <h3 class="card-title">Edit Questions</h3>
                          </div>
                          <!-- /.card-header -->
                          <!-- form start -->
                          <form role="form" id="editQuestionForm">
                              
                              <input type="hidden" id="rowId" >


                              <!-- <span id="d1" ></span> -->
                              <div class="card-body" style="margin: 18px 0px;">
                                  <div class="" id="textType">
                                      <div class="form-group">
                                          <label>Question Label </label>
                                          <input type="text" onblur="quest_vali(this)" class="form-control"
                                              id="edTextLable" placeholder="Please Enter Question Lable" maxlength="400">
                                      </div>


                                      <div class="form-group">
                                          <div class="col-md-6">
                                              <div class="row">
                                                  <label for="">X Coordinate</label>
                                                  <input type="text" class="form-control" id="edXCordinateInp" value="">
                                              </div>
                                          </div>

                                          <div class="col-md-6">
                                              <div class="row">
                                                  <label for="">Y Coordinate</label>
                                                  <input type="text" class="form-control" id="edYCordinateInp" value=""
                                                      required="">
                                              </div>
                                          </div>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <label>Select Question Type</label>
                                      <select class="form-control select2" style="width: 100%;" id="edQuestionType" onchange="showmapping(this)">
                                          <option selected="selected" value="">Select Question Type</option>
                                          <option value="text">Text Type</option>
                                          <option value="date">Date Type</option>
                                          <option value="label">Label</option>
                                          <option value="radio">Radio Button</option>
                                          <option value="checkbox">Checkbox</option>
                                          <option value="select">Drop Down</option>
                                      </select>
                                  </div>

                                  <div class="form-group" style="display:none" id="mappingDiv">
                                            <label>Select mapping</label>
                                            <select class="form-control select2" style="width: 100%" id="mappingType" onchange="edgetmapping(this)">1
                                                <option selected="selected" value="0">Select Mapping</option>
                                                <option value="name">Name</option>
                                                <option value="email">E-mail</option>
                                                <option value="mobile">Contact No</option>
                                                <option value="address">Address</option>
                                            </select>
                                        </div>

                                  <!-- <span><b id="d1"></b></span> -->
                                  <div class="form-check" id="edTextRequiredDiv">
                                      <input type="checkbox" class="form-check-input" id="edTextRequired">
                                      <label class="form-check-label" for="edTextRequired">Is Required</label>
                                  </div>

                                  <div class="form-check" id="ischildDiv">
                                      <input type="checkbox" class="form-check-input" id="edIschild"
                                          onchange="edChildCheck(this)">
                                      <label class="form-check-label" for="edIschild">Is Child</label>
                                  </div>


                                  <div class="form-check" id="isparentDiv">
                                      <input type="checkbox" class="form-check-input" id="edIsParent"
                                          onchange="edParentCheck(this)">
                                      <label class="form-check-label" for="edIsParent">Is Parent</label>
                                  </div>
                                  <hr>
                                  <div id="selectOption" class="hide">
                                      <div class="form-group">
                                          <label>Select Parent</label>
                                          <select class="form-control select2" style="width: 100%;" id="edParentType"
                                              onchnage="check_value(this)">
                                              <option selected="selected" value="">-- Select Parent --</option>
                                          </select>
                                          <p class="text-center" id="parName"></p>
                                      </div>

                                      <hr>
                                  </div>

                                  <div id="fontSettingsDiv">
                                      <div class="form-group">
                                          <label>Step No </label>
                                          <input type="text" class="form-control" id="edStepWizard"
                                              placeholder="Enter step wizard no" name="step" maxlength="2"
                                              onblur="step_valid(this)" onkeypress="return isNumber(event)">
                                      </div>
                                  </div>
                              </div>
                              <!-- /.card-body -->

                              <div class="card-footer">
                                  <button type="button" id="editQuestionBtn"  class="btn btn-primary">Save
                                      Question</button>
                              </div>
                          </form>
                      </div>

                  </div>
                </div> 
              </div>

              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
  </div>


  <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script src="plugins/iCheck/icheck.min.js"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


<script type="text/javascript" src="plugins/colorpicker/js/colorpicker.js"></script>
<script src="dist/js/pages/wizardValidation.js"></script>
<script>


$(document).ready(function() {
    var appid = `<?php echo $_REQUEST['id']; ?>`;
    if(appid){
        editQuestion(appid);
    }
});


  </script>

