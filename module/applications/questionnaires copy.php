  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="bootstrap/css/custom.css">


    <section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist" id="steps">
                </ul>
            </div>

            <form role="form" id="subForm">
                <div class="tab-content" id="tabcontent">
                <span class="block"></span>
                    <div class="tab-pane" role="tabpanel" id="complete"></div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </section>
 

<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>

$(document).ready(function(){
    
    var appid = `<?php echo $_REQUEST['applicationId']; ?>`
   var inputJson = {
       getQuestion:true,
       applicationId : `<?php echo $_REQUEST['applicationId']; ?>`
   }


    $.ajax({
        url : "config/ajax.php",  
        type: "post", //request type,
        dataType: 'json',
        data: {getstep:appid},      
        success: function(response) {
         
            if(response[0].step == 'null'){
                alert('null')
            }
           // return false;
        // let maxStep = response[0].step;        
        if(response[0].step){
            let maxStep = response[0].step;
            for(let i=0 ; i< maxStep; i++){
                let count = i+1;
                let link = '#step'+count;
                let control = 'step'+count;
                let cls = ''
                
                
                if(count == 1){
                    cls = 'active'

                }else{
                    cls = 'test'
                }
              
                $("#steps").append('<li role="presentation" class="'+cls+'"><a href="'+link+'" data-toggle="tab" data-step="'+count+'" onclick="step_valid(this)" aria-controls="'+control+'" role="tab" title="Step '+count+'"><span class="round-tab">'+count+'</span></a></li>');

                if(i == (maxStep-1)){
                    $("#steps").append('<li role="presentation" class="'+cls+'"><a href="#step101" data-toggle="tab" data-step="101" onclick="step_valid(this)" aria-controls="step101" role="tab" title="Save"><span class="round-tab"><span class="fa fa-save"></span></></a></li>');
                }

                let textstep = 'textstep'+count;

                $("#tabcontent").append('<div class="tab-pane '+cls+'" role="tabpanel" id="'+control+'"><div id="'+textstep+'"></div><ul class="list-inline pull-right"><li></li></ul></div>');

                // <button type="button" class="btn btn-danger next-step">continue</button>

                if(i == (maxStep-1)){
                 
                    $("#tabcontent").append('<div class="tab-pane '+cls+'" role="tabpanel" id="step101"><div><h2>Are you sure to submit this form ? </h2></div><ul class="list-inline pull-right"><li><button class="btn btn-danger" id="submitbtn">Submit</button></li></ul></div>');
                }

                $.ajax({
                    url : "config/ajax.php",  
                    type: "post", //request type,
                    dataType: 'json',
                    data: {id:appid,steps:count},      
                    success: function(response) {
                      
                        
                            $.each(response,function(key, value){
                                let text1 ='';
                                let divId = $('#step'+value.step);
                               
                                if(parseInt(value.isRequired) == 1){
                                    check = 'display:initial'
                                }else{
                                    check = 'display:none'
                                }
                                let name = "name"+value.id;
                                if(value.questionType == 'text'){
                                     text1 = '<div class="form-group"><label class="lab">'+ value.questionLable +'</label><span class="req" style="'+check+'">*</span><input type="'+value.questionType+'" class="form-control" name="'+name+'" id="'+value.id+'"></div>'
                                }
                                if(value.questionType == 'checkbox'){
                                      text1 = '<div class="form-check"><input class="form-check-input" name="'+name+'" type="'+ value.questionType +'" id="'+ value.id +'" > <label class="form-check-label" for="'+value.id+'">'+value.questionLable +'</label><span class="req" style="'+check+'">*</span></div>'
                                }
                                if(value.questionType == 'radio'){
                                    text1 = '<div class="form-check"><input class="form-check-input" name="'+name+'" type="'+ value.questionType +'" id="'+ value.id +'"> <label class="form-check-label" for="'+ value.id +'">'+value.questionLable +'</label><span class="req" style="'+check+'">*</span></div>';
                                }
                                divId.append(text1)

                        })
                    }
                });
            };  
        }

        
        }
    });
});

</script>