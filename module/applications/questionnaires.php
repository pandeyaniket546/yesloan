  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

  <link rel="stylesheet" href="bootstrap/css/custom.css">




  <div class="container">


      <div class="row" style="margin-right: 28px;padding: 7px;">
          <div class="col-12">
              <div class="card">
                  <div class="card-header">
                      <h3 class="card-title pull-left">Preview Questionnaire</h3>
                      <a href="?action=applicationList" class="btn btn-info pull-right">View All Application</a>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                      <div class="alert myWarning" role="alert">
                          No question found for this question . Click here to add question <a
                              href="?action=singleApplication&id=<?php echo $_REQUEST['applicationId']; ?>"><b>Add
                                  Questions</b><a>
                      </div>

                      <div class="row">
                          <div class="col-md-3" style="text-align: center;padding: 15px;"><label>Questionnaire
                                  Url</label>
                          </div>
                        <!-- //////////////// -->
                          
                          <div class="col-md-9" style="text-align: center;padding: 15px;">
                          <div class="input-group">
                            <textarea id="bar" readonly cols="67"  autocomplete="off" autocorrect="off" autocapitalize="off"
                                spellcheck="false"><?php echo $_SERVER['HTTP_HOST']."/short-app/questionnaires.php?applicationId=".$_REQUEST['applicationId']."&guestId=".rand();?></textarea>
                         </div>
                            <div class="form-actions">
                                <button class="btn btn-primary" type="button" onclick="clipboard()">
                                    Copy to Url
                                </button>
                            </div>

                            </div> 
                        <!-- /////////////// -->

                      </div>
                      <div class="stepwizard" id="stepwizardContainer">
                          <div class="stepwizard-row setup-panel">
                          </div>
                      </div>

                      <form role="form" id="subForm" autocomplete="off" autocorrect="off" autocapitalize="off" >
                          <input type="hidden" value="<?php echo ($_SESSION['MEMBER_ID']); ?>" name="memberid" />

                          <input type="hidden" name="applicationid" value="<?php echo $_REQUEST['applicationId']; ?>" />
                      </form>

                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">

                  </div>

              </div>
              <!-- /.card-body -->
          </div>
      </div>
  </div>



  </div>

  <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
   
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script src="plugins/iCheck/icheck.min.js"></script>
  <!-- <script src="bootstrap/js/custom.js"></script> -->
  <script src="dist/js/sketch.js"></script>

<script src="dist/js/jquery.signfield-en.min.js"></script>
<script src="dist/js/jquery.signfield.min.js"></script>
  <script src="dist/js/pages/wizardValidation.js"></script>
  <script src="plugins/datepicker/bootstrap-datepicker.js"></script>

 
<!-- <script src="dist/js/custom.js"></script> -->





  <script>
$(document).ready(function() {
    var appid = `<?php echo $_REQUEST['applicationId']; ?>`
    var guestId = `<?php echo isset($_REQUEST['guestId']); ?>`
    if(guestId){
        guestId = 'success';
    }else{
        guestId = 'Fail'
    }   

    getWizardData(appid, guestId);
    setTimeout(function(){
       $('.datepicker').datepicker({
        autoclose : true,
        format: 'mm-dd-yyyy',
       }); 
    }, 2000);

   

});

//Date picker

 function clipboard(){  
    var appid = `<?php echo $_REQUEST['applicationId']; ?>`
    copyToClipboard(document.getElementById("bar"));  
    updateQuetionnaireUrl("bar",appid);  
 }

  </script>