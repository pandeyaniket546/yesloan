
 <link rel="stylesheet" href="bootstrap/css/custom.css">

<section class="content">
    <div class="row" style="margin-left: -7px;padding: 7px;">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-left">All Applications</h3>
                    <a href="?action=newApplication" class="btn btn-info pull-right">Add New Application</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body pad-around">
                    <table id="allApplication" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Application Name</th>
                                <th>Total pages</th>
                                <th>File Name</th>
                                <th>Created At</th>                               
                                <th>Pdf access</th>
                                <th>Edit</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                $data = fetch_multiple('newApplications','*','','');
                if($data){
                 foreach($data as $result){
                   
                 $createdAt = date_create($result['createdAt']);
                        $count = count_question($result['id']);
                        $count = ($count['count']);
                        
                     ?>
                            <tr>
                                <td><?php echo $result['applicationName'];?></td>
                                <td> <?php echo $result['totalPages'];?></td>
                                <td><?php echo $result['originalFileName'];?></td>
                                <td> <?php echo date_format($createdAt,"m-d-Y H:i:s");?></td>
                                <td><?php if($result['isPdfGenerate'] == 1){ ?>
                                        <span class="badge bg-green">User</span>
                                    <?php }else{ ?>
                                        <span class="badge bg-yellow">Admin</span>
                                    <?php } ?>
                                </td>
                                <td><Button onclick="getEditNapplication(<?php echo $result['id'];?>)" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i></button</td>
                                <td>
                                    <a target="_blank" href="?action=singleApplication&id=<?php echo $result['id'];?>">Add Questionaries</a> /
                                    <?php if($count > 0) { ?>
                                    <a target="_blank" href="?action=allQuestions&applicationId=<?php echo $result['id'];?>">Preview
                                        Questionaries</a> /
                                    <a target="_blank" href="?action=editQuestion&id=<?php echo $result['id'];?>">Edit
                                        Questionaries</a> /
                                   
                                    <?php } ?>
                                        <a class="addCurser" onclick ="deleteApplication(<?php echo $result['id'];?>)">Delete
                                        Questionaries</a>


                                </td>
                                
                            </tr>
                            <?php 
                 }
                }else{
                    ?>
                            <tr>
                                <td colspan="5"><?php echo NO_DATA_AVAIL ; ?></td>
                            </tr>
                            <?php 
                }
                ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
    </div>

    <div class="modal" id="modal" tabindex="-1" role="dialog" >
    <link href="plugins/colorpicker/bootstrap-colorpicker.css" rel="stylesheet">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Application Details
                     <button onclick="modalcls()" type="button" class="close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </h3>        
                </div>
                <div class="modal-body">
                  <form id="newEdiApplicaForm">
                  <input type="hidden" name="upAddliId" id="upAddliId">
                    <div class="form-group">
                        <label class="col-form-label">Application Name :</label>
                        <input type="text" maxlength="100" minlength="3" class="form-control" required id="name" name="name">
                    </div>

                    <div class="form-group" style="display: flow-root;">                  
                        <label>Select Font Color </label>
                        <input readonly type="text" class="form-control" id="fontclr" name="fontColor">
                        <span id="showcolor"><span>
                    </div>

                    <div class="form-group">
                        <label>Select Font Family</label>
                            <select class="form-control select2" style="width: 100%;" id="FontFamilyE"
                                name="selectedFont">
                                <option selected="selected" value="">Select Font Family</option>
                                <option value="Arial">Arial</option>
                                <option value="Helvetica">Helvetica</option>
                                <option value="Verdana">Verdana</option>
                                <option value="Calibri">Calibri</option>
                                <option value="Noto">Noto</option>
                                <option value="Lucida Sans">Lucida Sans</option>
                                <option value="Gill Sans">Gill Sans</option>
                                <option value="Century Gothic">Century Gothic</option>
                                <option value="Candara">Candara</option>
                                <option value="Futara">Futara</option>
                                <option value="Franklin Gothic Medium">Franklin Gothic Medium</option>
                                <option value="Trebuchet MS">Trebuchet MS</option>
                                <option value="Geneva">Geneva</option>
                                <option value="Segoe UI">Segoe UI</option>
                            </select>                                         
                    </div>

                    <div class="form-check" style="margin-bottom: 20px">
                        <input type="checkbox" class="form-check-input" id="pdfCheckoxE" name="pdfCheckoxE">
                        <label class="form-check-label" for="pdfCheckoxE">Pdf Generated by Questionnaires User</label>
                    </div>

                </div>
                <div class="modal-footer">        
                    <button type="submit" class="btn btn-primary" id="addNewEditApplicatiion">Submit</button>
                </div>
            </form>
    </div>
    <script>
        function modalcls(){
           $('#modal').hide();
        } 
    </script>
  </div>
    

    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script>


   function deleteApplication(id){ 
       
       if(confirm('Are you sure want to delete this application data?')){
        $.ajax({
          url:"config/ajax.php", //the page containing php script
          type: "post", //request type,
          dataType: 'json',
          data: {deleteApplication :true ,applicationId:id},
          success:function(result){
    //    console.log('result')
          $.notify({
            title: '<strong> Application Deleted Successfuly !</strong>',
            message: '',      
          },{
            delay: 2000,
            z_index:999999999,
            type: 'success',
          });
          location.reload();
         },
         error: function(result){
    $.notify({
            title: '<strong>Something went wrong!</strong>',
            message: '',      
          },{
            delay: 2000,
            z_index:999999999,
            type: 'error',
          });
          console.log('error',result);
         }
       });

       }else{
           console.log('cancel');
       }
   }
   

   function getEditNapplication(newAppliid){
        console.log('yes....')
       if(newAppliid){
        $.ajax({
          url:"config/ajax.php", //the page containing php script
          type: "post", //request type,
          dataType: 'json',
          data: {editNewApplication :true ,newAppliid:newAppliid},
          success:function(result){
              console.log('result new application.....',result[0])
              if(result[0]){
                  $('#name').val(result[0].applicationName)
                  $('#upAddliId').val(result[0].id)
                  $('#showcolor').css("background-color", "rgb("+result[0].fontColor+")");
                  $('#fontclr').colorpicker({
                    format: 'rgb'
                  })
                  $('#fontclr').val(result[0].fontColor)
                  $("select#FontFamilyE option").filter(function() {          
                    return $(this).val() == result[0].fontFamily;
                  }).attr('selected', true);
                 
                  if(result[0].isPdfGenerate == 1){
                    $('#pdfCheckoxE').prop('checked', true);
                  }else{
                    $('#pdfCheckoxE').prop('checked', false);
                  }
                  $('#modal').show();
              }
          }
        });
       }

   }



   </script>
   <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="dist/js/pages/wizardValidation.js"></script>


<script>
$(document).ready(function() {    
    $('#allApplication').DataTable();
    $('#modal').hide();
});


$("form#newEdiApplicaForm").submit(function(e){   
    e.preventDefault();
    let name = $('#name').val();
    let upAddliId = $('#upAddliId').val();
    let fontclr = $('#fontclr').val();
    let FontFamilyE = $('#FontFamilyE').val();
    let pdfCheckoxE = '';

    if ($('#pdfCheckoxE').is(":checked")){
        pdfCheckoxE = 1;
    }else{
        pdfCheckoxE = 0 ;
    }
    let inputjson = {
        updateNwApplication :true,
        name:name,
        upAddliId:upAddliId,
        fontclr:fontclr,
        FontFamilyE:FontFamilyE,
        pdfCheckoxE:pdfCheckoxE,        
    }

    if((name.trim()).length > 0){
        $.ajax({
          url:"config/ajax.php", //the page containing php script
          type: "post", //request type,
          dataType: 'json',
          data: inputjson,
          success:function(result){
              console.log('Result...11...',result)
              if(result.status == 200){
                $.notify({     
                  title: '<strong> Application Updated successfuly !</strong>',
                  message: '',      
                 },{
                  delay: 3000,
                  z_index:999999999,
                  type: 'success',    
                });
                $('#modal').hide();
                location.reload();
              }
          }
        })
    }
})


</script>
