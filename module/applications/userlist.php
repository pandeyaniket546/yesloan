  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

  <link rel="stylesheet" media="screen" type="text/css" href="plugins/colorpicker/css/colorpicker.css" />

  <link rel="stylesheet" href="bootstrap/css/custom.css">
  
  <section class="content">
    <div class="row" style="margin-left: -7px;padding: 7px;">
        <div class="col-12">
            <div class="card">
                    <div class="card-header">
                       <h3 class="card-title pull-left">All Applications</h3>
                    </div>

                    <div class="card-body pad-around">
                        <div class="form-group">
                            <label>Search Application</label>
                            <select class="form-control select2" style="width: 100%;"
                                id="SearcApp" name="selectedFont" onchange="getAllUser(this)">
                                <option selected="selected" value="">Search Application</option>                               
                            </select>
                        </div>

                        <div class="alert alert-dismissible alert-info" id="alertbox">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4 class="alert-heading">Not found any user in this Application</h4>
                            <p class="mb-0 hide">Best check yo self, you're not looking too good. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna, <a href="#" class="alert-link">vel scelerisque nisl consectetur et</a>.</p>
                        </div>     

                        <!-- //////////////////////////////////////////// -->
                        
                            <table class="table table-hover" id="table_id">
                                <thead>
                                    <tr>
                                        <th scope="col">S. No</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Contact No.</th>
                                        <!-- <th scope="col">Address</th> -->
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="edques">
                                </tbody>
                            </table>
                        
                   <!-- //////////////////////////////////////////// -->



                    </div>
            </div>
        </div>
    </div>
    </section>

  


  <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script src="plugins/iCheck/icheck.min.js"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


<script type="text/javascript" src="plugins/colorpicker/js/colorpicker.js"></script>
<script src="dist/js/pages/wizardValidation.js"></script>
<script>


$(document).ready(function() {
        
        $('#table_id').hide();
        $('#alertbox').hide();
        getAllApplication();
        $('.select2').select2();      
   
});

function getAllUser(e){
    //console.log('this is user list');
    let id = $(e).val();
    console.log(id);
    if(id !==""){
        $.ajax({
            url: "config/ajax.php",
            type: "post", //request type,
            dataType: 'json',
            data: {getallusr: true, appliId:id},
            success: function(response) {
                console.log(response);
            // console.log('Response 123.........',response);
            // console.log('Response query.........',response.query);
                if(response.status !== 400){
                    $("#table_id > tbody").html("");
                    $.each(response, function(key, value) {
                        let count = key + 1
                        $('#edques').append('<tr><td>' + count + '</td><td>' + value.name +
                            '</td><td>' + value.email +'</td><td>' + value.mobile +'</td><td><a target="_blank" href="/short-app/questionnairesAnswer.php?applicationId='+value.applicationId+'&newuserid='+value.newuserid+'" class="btn btn-primary">View</a><a target="_blank" href="/short-app/assest/answers/'+value.pdfName+'" class="btn btn-primary" style="margin-left: 10px;">Download</a></td></tr>')

                        // $('#edques').append('<tr><td>' + count + '</td><td>' + value.name +
                        //     '</td><td>' + value.email +'</td><td>' + value.mobile +'</td><td>' + value.address +'</td><td><a target="_blank" href="/sba-app/generateAnswer.php?applicationId='+value.applicationId+'&newuserid='+value.newuserid+'" class="btn btn-primary">View</a></td></tr>')
                    }) 
                   // applicationId=57&guestId=1679273136

                    $('#alertbox').hide();
                    $('#table_id').show();
                    $('#table_id_wrapper').show();
                    $('#table_id').DataTable({
                        dom: 'Bfrtip',
                        retrieve: true,
                    });
                }else{
                    $('#alertbox').show();
                    $('#table_id').hide();
                    $('#table_id_wrapper').hide();
                    $("#table_id > tbody").html("");
                }

            }
        });
    }

}



  </script>

