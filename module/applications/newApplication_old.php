<?php 

require(__DIR__ . "/../../vendor/autoload.php");
   
     if(isset($_POST['addApplication']) && (isset($_POST['applicationName']) && !empty($_POST['applicationName']))){
 
      if(isset($_POST['fontColor'])){
        $fontColor = $_POST['fontColor'];
        $fontColor = trim($fontColor);
          if($fontColor){
            $fontColor = str_replace('rgb(', '',$fontColor);
            $fontColor = str_replace(')', '',$fontColor); 
            $fontColor = (explode(",",$fontColor));
            if($fontColor[0] && $fontColor[1] && $fontColor[2] ){
              $fontColor = $fontColor[0].','.$fontColor[1].','.$fontColor[2];
            }else{
              $fontColor = '';
            }
          }else{
            $fontColor = '';
          }        
      }else{
        $fontColor = '';
      }

      if(isset($_POST['selectedFont'])){
        $fontFamily = $_POST['selectedFont'];
      }else{
        $fontFamily = '';
      }
      if(isset($_POST['pdfCheckox'])){
        $isPdfGenerate = 1;
      }else{
        $isPdfGenerate = 0;
      }   

     
        $originalFileName =  $_FILES["originalFile"]["name"];
        $target_dir = "assest/originalFile/";
        $img_target_dir = "assest/convertedImages/";
       
        $pdfAbsolutePath = "/assest/originalFile/".$originalFileName;
        $target_file = $target_dir . basename($_FILES["originalFile"]["name"]);
        //upload original input file 
 
        if (move_uploaded_file($_FILES["originalFile"]["tmp_name"], $target_file)) { 
            
           chmod($target_file, 0666); 
            
            $pdf = new Spatie\PdfToImage\Pdf("assest/originalFile/" . $originalFileName);
            $noOfPages  = $pdf->getNumberOfPages();
           
            $data = array(
                'applicationName' => $_POST['applicationName'],
                'totalPages' => $noOfPages,
                'originalFileName'=>$originalFileName,
                'fontColor'=>$fontColor,
                'fontFamily'=>$fontFamily,
                'isPdfGenerate' => $isPdfGenerate
                );
           
            //Save application data

            $applicationId = insert('newApplications',$data);

        for($i = 1; $i <= $noOfPages; $i++) { 
            $img = ($i).'-'.rand().'.jpg'; 
            $applicationDetails = array(
            'applicationId'=>$applicationId,
            'convertedImages'=>$img
            );
           
            $pdf->setPage($i)
            ->saveImage("./assest/convertedImages/".$img);
           
            //save converted images 
            insert('applicationDetails',$applicationDetails);
        
        }
        if($applicationId){
          //  echo "Application added Successfully!";
            display_error('alert-success','Application added successfully!');
            ?>
            <script>
             setTimeout(function(){
              window.location.href = "home.php?action=singleApplication&id="+<?php echo $applicationId; ?>;
                 }, 1000);
            
            </script>
            <?php    
        } 
 }else{
  display_error('alert-error','Please try later!'); 
 }
     }

?>
 <link href="plugins/colorpicker/bootstrap-colorpicker.css" rel="stylesheet">
 <section class="content">
      <div class="row" style="margin-left: -7px;padding: 7px;">
        <div class="col-12">
<div class="card">
            <div class="card-header">
              <h3 class="card-title pull-left">Add New Applications</h3>
              <a href="?action=applicationList" class="btn btn-info pull-right">View All Application</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <form method="POST" style="padding: 20px;" enctype="multipart/form-data" >
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Application Name</label>
                    <input type="text" name="applicationName"  class="form-control" id="exampleInputEmail1" placeholder="Enter Application Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Upload Application File</label>
                    <input type="file" name="originalFile"  class="form-control">
                  </div>

                  <div class="form-group" id="fontclr">                  
                    <label>Select Font Color </label>
                    <input readonly type="text" class="form-control my-colorpicker1" id="fontColor" name="fontColor">
                  </div>

                  <div class="form-group">
                    <label>Select Font Family</label>
                    <select class="form-control select2" style="width: 100%;"
                        id="fontFamily" name="selectedFont">
                        <option selected="selected" value="">Select Font Family</option>
                        <option>Arial</option>
                        <option>Helvetica</option>
                        <option>Verdana</option>
                        <option>Calibri</option>
                        <option>Noto</option>
                        <option>Lucida Sans</option>
                        <option>Gill Sans</option>
                        <option>Century Gothic</option>
                        <option>Candara</option>
                        <option>Futara</option>
                        <option>Franklin Gothic Medium</option>
                        <option>Trebuchet MS</option>
                        <option>Geneva</option>
                        <option>Segoe UI</option>
                        <option>Cursive standard Bold</option>
                    </select>
                  </div>

                  <div class="form-check" style="margin-bottom: 20px">
                    <input type="checkbox" class="form-check-input" id="pdfCheckox" name="pdfCheckox">
                    <label class="form-check-label" for="pdfCheckox">Pdf Generated by Questionnaires User</label>
                  </div>
                
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button  onclick="submitform()" id="addApplicationNew" class="btn btn-primary">Submit</button>
                  <button type="submit" id="addApplication"  name="addApplication" class="btn hide">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
          </div>
          </div>
          </div>
          <!-- plugins\colorpicker\bootstrap-colorpicker.css -->
         
          <script src="//code.jquery.com/jquery-3.4.1.js"></script>
         
<script>
    function submitform(){
      $('#addApplicationNew').attr('disabled', true);
      $("#addApplication").trigger('click');
    }


    $(document).ready(function(){
      $('.select2').select2();

      $('#fontColor').colorpicker({    
        format: 'rgb',
      }).on()
    });
</script>