
<?php 
ob_start();
require(__DIR__ . "/../../vendor/autoload.php");
$id = $_REQUEST['id'];
$img_target_dir = "assest/convertedImages/";
$applicationData= fetch_single('newApplications','*','id',$id);
$applicationDetail = fetch_multiple_with_key('applicationDetails','*','applicationId',$id);
?>
<link rel="stylesheet" media="screen" type="text/css" href="plugins/colorpicker/css/colorpicker.css" />
<link rel="stylesheet" type="text/css" href="plugins\Easy-Searchable-Filterable-Select-List-with-jQuery\jquery.searchableSelect.css" />
<!-- C:\xampp\htdocs\php_latest\sba-app\plugins\Easy-Searchable-Filterable-Select-List-with-jQuery\jquery.searchableSelect.css -->
<section class="content">
    <div class="row" style="margin-left: -7px;padding: 7px;">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-left">Application</h3>
                    <a href="?action=applicationList" class="btn btn-info pull-right">View All Application</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form role="form" id="quickForm" method="post" style="padding: 20px;" enctype="multipart/form-data">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Application Name</label>
                                <input type="text" name="applicationName" readonly
                                    value="<?php echo $applicationData['applicationName'];?>" required
                                    class="form-control" id="exampleInputEmail1" placeholder="Enter Application Name">
                            </div>
                            <div class="row">
                                <?php 
                                foreach($applicationDetail as $res){
                                    if($res){
                                ?>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <?php 
                                            $imgname = $res['convertedImages'];
                                            $pagenumber = explode("-",$imgname)
                                        ?>
                                        <img src="assest/convertedImages/<?php echo $res['convertedImages']; ?>"
                                            id="<?php echo $res['id']; ?>" class="applicationImg" style="width:100%" data-pagenumber="<?php echo $pagenumber [0]; ?>">
                                    </div>
                                </div>
                                <?php 
                                    }
                            }
                            ?>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
    </div>

    <!---------Modal for view image ---------->

    <div class="container">

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog" style="width:97%">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Questions</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-9">
                               <div style="width:100%;overflow:scroll" id="imgWrapper">
                                <img src="" id="convertedImg" style="width:auto" />
                                </div>
                            </div>
                            <div class="col-md-3">


                                <div class="row">
                                    <div class="col-md-6">
                                        <a class="btn btn-info"  id="viewQuestionBtn"  target="_blank" href="?action=editQuestion&id=<?php echo $id;?>">Preview Question List</a>
                                    </div>

                                    <div class="col-md-6">
                                        <a class="btn btn-info hide" id="addNewQuestionBtn">Add New Question</a>
                                    </div>
                                </div>                              


                            </div>

                            <!------Cordinates X ------------>
                            <div class="card card-primary" id="addQuestionsDiv" style="padding:8px">
                                <div class="card-header">
                                    <h3 class="card-title">Add Questions</h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->

                                <form role="form" id="addQuestionForm">

                                    <input type="hidden" id="applicationId" value="<?php echo $_REQUEST['id']; ?>">
                                    <input type="hidden" id="imageId">
                                    <input type="hidden" id="xCordinate">
                                    <input type="hidden" id="yCordinate">
                                    <input type="hidden" id="fontColor">
                                    <input type="hidden" id="fontFamily">
                                    <!-- <span id="d1" ></span> -->
                                    <div class="card-body" style="margin: 18px 0px;">
                                        <div class="" id="textType">
                                            <div class="form-group hide">
                                                <label>Question Label </label>
                                                <input type="text" onblur="quest_vali(this)" class="form-control"
                                                    id="textLable" placeholder="Please Enter Question Lable"
                                                    maxlength="400">
                                            </div>

                                            <div class="form-group">
                                                <label>Select Question Label</label>
                                                <select class="form-control select2" style="width: 100%;" id="pagelabel">
                                                    <option selected="selected" value="">Select Question Label</option> 
                                                </select>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <label for="">X Coordinate</label>
                                                        <input type="text" class="form-control" id="xCordinateInp"
                                                            value="" readonly>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <label for="">Y Coordinate</label>
                                                        <input type="text" class="form-control" id="yCordinateInp"
                                                            readonly value="" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Select Question Type</label>
                                            <select class="form-control select2" style="width: 100%;" id="questionType">
                                                <option selected="selected" value="">Select Question Type</option>
                                                <option value="text">Text Type</option>
                                                <option value="date">Date Type</option>
                                                <option value="label">Label</option>
                                                <option value="radio">Radio Button</option>
                                                <option value="checkbox">Checkbox</option>
                                                <option value="select">Drop Down</option>
                                                <option value="signature">Signature</option>
                                            </select>
                                        </div>

                                        <div class="form-group" style="display:none" id="mappingDiv">
                                            <label>Select mapping</label>
                                            <select class="form-control select2" style="width: 100%" id="mappingType" onchange="getmapping(this)">1
                                                <option selected="selected" value="">Select Mapping</option>
                                                <option value="name">Name</option>
                                                <option value="email">E-mail</option>
                                                <option value="mobile">Contact No</option>
                                                <option value="address">Address</option>
                                            </select>
                                        </div>

                                        <!-- <span><b id="d1"></b></span> -->
                                        <div class="form-check" id="txtrequired">
                                            <input type="checkbox" class="form-check-input" id="textRequired">
                                            <label class="form-check-label" for="textRequired">Is Required</label>
                                        </div>

                                        <div class="form-check" id="ischildDiv" style="display:none">
                                            <input type="checkbox" class="form-check-input" id="ischild"
                                                onchange="childCheck(this)">
                                            <label class="form-check-label" for="ischild">Is Child</label>
                                        </div>


                                        <div class="form-check" id="isparentDiv" style="display:none">
                                            <input type="checkbox" class="form-check-input" id="isParent"
                                                onchange="parentCheck(this)">
                                            <label class="form-check-label" for="isParent">Is Parent</label>
                                        </div>
                                        <hr>
                                        <div id="selectOption" style="display:none">
                                            <div class="form-group">
                                                <label>Select Parent</label>
                                                <select class="form-control select2" style="width: 100%;"
                                                    id="parentType" onchange="check_value(this)">
                                                    <option selected="selected" value="">-- Select Parent --
                                                    </option>
                                                </select>
                                            </div>
                                            <hr>
                                        </div>

                                        <div id="fontSettingsDiv" style="display:none">
                                            <div class="form-group">
                                                <label>Step No </label>
                                                <input type="text" class="form-control" id="stepWizard"
                                                    placeholder="Enter step wizard no" name="step" maxlength="2"
                                                    onblur="step_valid(this)" onkeypress="return isNumber(event)">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->

                                    <div class="card-footer">
                                        <button type="button" id="addQuestionBtn" disabled class="btn btn-primary">Add
                                            Question</button>
                                    </div>
                                </form>
                            </div>

                            <!------ Cordinates Y ---------->

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </div>
    



<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="plugins/colorpicker/js/colorpicker.js"></script>

<script src="plugins\Easy-Searchable-Filterable-Select-List-with-jQuery\jquery-1.11.1.min.js"></script>

<script src="plugins/Easy-Searchable-Filterable-Select-List-with-jQuery\jquery.searchableSelect.js"></script>



<script>
$(document).ready(function(){
  
    $('.select2').select2({   
        tags: true
    });

    let id = <?php echo $_REQUEST['id']; ?>

    if(id){
        var inputJson = {
            getfont:"yes",
            fontappid: id,
        }

        $.ajax({
            url:"config/ajax.php", //the page containing php script
            type: "post", //request type,
            dataType: 'json',
            data: inputJson,
            success:function(result){
            
            console.log('result.......',result)
            let data  = result[0];
            let fontFamily = data['fontFamily'];
            let fontColor = data['fontColor']
            $('#fontFamily').val(fontFamily)
            $('#fontColor').val(fontColor)            
            },

        });
    }    
});
</script>  
