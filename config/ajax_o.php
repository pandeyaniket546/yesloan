<?php 
error_reporting(E_ALL);
ini_set('display_errors', '1');
require("database.php");
require("constant.php"); 
require("helper.php");

//Ajax call for save questions for a application
if(isset($_POST['addQuestion'])){ 
      $textlabel = $_POST['textLable'];
      $textlabel = str_replace("'", "\'", $textlabel);

    $data = array(
        'applicationId'=>$_REQUEST['applicationId'],
        'imageId' => $_POST['imageId'],
        'xCordinates' => $_POST['xCordinate'],
        'yCordinates'=>$_POST['yCordinate'],
        'questionType' => $_POST['questionType'],
        'mappingName' => $_POST['mappingName'],
        'fontColor' => $_POST['fontColor'],
        'fontFamily'=>$_POST['fontFamily'], 
        'questionLable'=>$textlabel,
        'isRequired'=>$_POST['textRequired']=='on' ? 1 : 0  ,
        'isParent'=>$_POST['isParent']=='on' ? 1 : 0 ,
        'parent_id'=>$_POST['parentType']!=''?$_POST['parentType'] : 0,
        'isChild'=>$_POST['ischild']=='on' ? 1 : 0 ,
        'step'=>$_POST['stepWizard']!='' ? $_POST['stepWizard'] : 1 
        );

      // echo '<pre>';  print_r($data); die;
       $save = insert('questions',$data);
      if($save){
        echo json_encode(array("status"=>'question added successfuly'));
      }else{
        echo json_encode(array("status"=>'failed'));
      }
      
}

//Ajax call for get all parent question by applicationId
if(isset($_POST['getParent'])){
  $appIdforparent = $_POST['appIdforparent'];
  if($appIdforparent){
    $sql = "select * from questions where isParent=1 AND applicationId=".$appIdforparent;
    $data = fetch_custom($sql);
    if($data){
      echo json_encode($data);
    } 
  }
       
}


//Ajax call for get all question by applicationId
if(isset($_POST['getQuestion'])){
  $applicationId = $_POST['applicationId'];
  $sql = "select * from questions Where applicationId = $applicationId";
    $data = fetch_custom($sql);
    if($data){
      echo json_encode($data);
    }      

}

//Ajax call for getting questions details by questionId and ImageId
if(isset($_POST['getQuestionByImageId'])){
  $applicationId = $_POST['applicationId'];
  $imageId = $_POST['imageId'];

  $sql = "SELECT * FROM questions WHERE applicationId = $applicationId AND imageId = $imageId";
    $data = fetch_custom($sql);
    if($data){
      echo json_encode($data);
    }      

}

//Ajax call for getting result by questionId for pagination

if(isset($_POST['getQuestionById'])){
  $questionId = $_POST['id'];
  $resultOf = $_POST['resultOf'];
  $questionId = $resultOf=='next' ? $questionId + 1 : $questionId - 1;
  $sql = "select * from questions Where id = $questionId";
    $data = fetch_custom($sql);
    if($data){
      echo json_encode($data);
    }  

}


// Get steps of specific application
if( isset($_POST['getstep']) ){
  $appid = $_POST['getstep'];
  $data1 ="";
  if(isset($_POST['getUsDetails'])){
    $getUsDetails = $_POST['getUsDetails'];
    $sql1 = "SELECT * FROM questionnaireUser where id=".$getUsDetails;
    $data1 = fetch_custom($sql1); 
  }
 $sql = "SELECT MAX(step) as step FROM questions where applicationId=".$appid;
 $data = fetch_custom($sql);
   
    if($data){
      if($data1){
        array_push($data,['userDetails'=>$data1]);
      }
      echo json_encode($data);
    }
 }


 // Get All Questions 
 if( isset($_POST['getappIdData']) ){
  $appid = $_POST['getappIdData'];
  $sql = "select * from questions where applicationId=".$appid;  
    $data = fetch_custom($sql);
    if($data){
       echo json_encode($data);
    } 
 }

 // Get step wise All question
 if(isset($_POST['id'])){   
  $id = ($_POST['id']);
  $steps = ($_POST['steps']);
  $sql = "select * from questions where applicationId=".$id." and step=".$steps;
  $data = fetch_custom($sql);
    if($data){
       echo json_encode($data);
    } 
}

// Get all details of perticular Question
if(isset($_POST['editAppId'])){
  $id = ($_POST['editAppId']); 
  $sql = "select * from questions where applicationId=".$id;
  $data = fetch_custom($sql);
    if($data){
       echo json_encode($data);
    } 
}

// get image of perticular appliction Id
if(isset($_POST['editId'])){
  $id = ($_POST['editId']); 
  // $sql = "select * from questions where id=".$id;
  $sql = "SELECT *,questions.id as editid, applicationDetails.id AS imgId, convertedImages as imgName FROM `questions` LEFT JOIN applicationDetails ON questions.imageId = applicationDetails.id WHERE questions.id=".$id;
  $data = fetch_custom($sql);
    if($data){
       echo json_encode($data);
    } 
}


// Save Edit question
if(isset($_POST['editQuestion'])){ 
  
  $data = array(
      'questionLable' => $_POST['textLable'],
      'xCordinates' => $_POST['xCordinate'],
      'yCordinates'=>$_POST['yCordinate'],
      'questionType' => $_POST['questionType'],     
      'questionLable'=>$_POST['textLable'],
      'mappingName'=>$_POST['mappingType'],
      'isRequired'=>$_POST['textRequired'] =='on' ? true : false ,
      'isParent'=>$_POST['isParent'] =='on' ? true : false,
//      'parent_id'=>$_POST['parentType']!=''?$_POST['parentType'] : 0,
      'isChild'=>$_POST['ischild'] =='on' ? true : false,
      'step'=>$_POST['stepWizard']!='' ? $_POST['stepWizard'] : 1 
      );
      $editAppId =  $_POST['editAppId'];    

      foreach( $data as $key => $value){        
        $res = update('questions',$key,$value,$editAppId);        
      }     
   
    if($res){
      echo json_encode(array("status"=>"Question Edited Successfully"));
    }else{
      echo json_encode(array("status"=>'failed'));
    }
    
}
// delete Question

if(isset($_POST['delId'])){
  $id = ($_POST['delId']); 
  $sql = "select * from questions where applicationId=".$id;
  $res = delete('questions','id',$id);
  if($res){
    echo json_encode(array("status"=> $res));
  }
}

// get application font and font family
if(isset($_POST['getfont'])){
  $id = ($_POST['fontappid']); 
  $sql = "select * from newApplications where id=".$id;
  $res = fetch_custom($sql);
  
  if($res){   
     echo json_encode($res);
    
  }
}

// Save Answer of a perticular application
if(isset($_POST['ches'])){  
    //  echo '<pre>'; print_r($_POST); die;
      $arr = $_POST['mainVal'];
      $memberid = $arr[0]['value'];
      $applicationId = $arr[1]['value'];
      $userId = $arr[2]['value'];

      $qname = $arr[3]['value'];
      $qemail = $arr[4]['value'];
      $qmobile = $arr[5]['value'];
      // $qaddress = $arr[6]['value'];
      $qaddress = "";
      $userdaataqu = array();
      if($qname && $qemail && $qmobile ){
        $sql = "UPDATE questionnaireUser SET name = '".$qname."', email='". $qemail."'  , mobile='". $qmobile."', address='". $qaddress."'  WHERE id=".$userId;
        array_push($userdaataqu,$sql);
        $result = $GLOBALS['conn']->query($sql);
      }
      $emprytest = array();
      $queryresponse = array();

      $date = date("Y-m-d H:i:s");
            /////////////////////////////////
            for ($i=0; $i< count($arr); $i++) { 
        
              $key = ($arr[$i]['name']);
              $value = ($arr[$i]['value']);            
              $key = preg_replace('/[^0-9]/', '',  $key);            
              $answerCheckQuery = "DELETE FROM answers WHERE memberId =  $memberid";
              $answerCheck = delete_custom($answerCheckQuery);
                
            }
            ///////////////////////////////

      for ($i=0; $i< count($arr); $i++) {   
            $key = ($arr[$i]['name']);
            $value = ($arr[$i]['value']);            
            $key = preg_replace('/[^0-9]/', '',  $key);    
            $data = fetch_single('questions','questionType','id',$key);      
            if($data){
              $quesType = $data['questionType'];
              $query =  "INSERT INTO answers (applicationId, questionType, questionId, answer,memberId,newUserId,createdat) VALUES ('$applicationId','$quesType', '$key', '$value','$memberid','$userId','$date')"; 
                 array_push($emprytest,$query);
              if ($GLOBALS['conn']->query($query) === TRUE) {         
                  $res = $GLOBALS['conn']->insert_id;
                  array_push($queryresponse,$res);
                }
            }
      }

      $pdfCheckQuery = "select * from newApplications Where id = $applicationId";
              $pdfCheckQuery = fetch_custom($pdfCheckQuery);
               if($pdfCheckQuery){ 
                $pdfCheckQuery = ($pdfCheckQuery[0]['isPdfGenerate']);
               }
               
      $data = array(
        'status' =>200,
        'message'=>'success',
        'applicationId'=>$applicationId,
        'pdfCheckQuery' =>$pdfCheckQuery,
        'insertQuery'=>$emprytest,
        'queryresponse'=>$queryresponse,
        'userdataquery'=>$userdaataqu,
    );
      echo json_encode($data);
}

// Get child of selec-box 
if(isset($_POST['parentid'])){
      $parentid = $_POST['parentid'];
      $sql = "select * from questions Where parent_id =  $parentid";
        $data = fetch_custom($sql);
        if($data){
          echo json_encode($data);
        }
    }

if(isset($_POST['deleteApplication'])){
  $applicationId = $_POST['applicationId'];
  $sql = "select * from newApplications Where id = $applicationId";
  $res = fetch_custom($sql);
  
  if($res){
    $status = delete_application($applicationId);
    // echo '<pre>'; print_r($status); die;
    if($status){
     echo json_encode($status);
    }
  }
  
}

//Ajax call for getting labels by page number
if(isset($_POST['getPageLabel'])){
  $applicationId = $_POST['appId'];
  $imageId = $_POST['imgId'];
  $pagenumber = $_POST['pagenumber']; 

  $sql = "SELECT * FROM newApplications WHERE id = $applicationId";
    $data = fetch_custom($sql);
    if($data){     
        require(__DIR__ . "/../vendor/autoload.php");
      // require(__DIR__ . "/../pdfparser-master/vendor/autoload.php");     
      $originalFileName = $data[0]['originalFileName'];    
      $parser = new \Smalot\PdfParser\Parser();
      $pdf = $parser->parseFile(__DIR__ ."/../assest/originalFile/".$originalFileName);
      $pages  = $pdf->getPages();

    // Loop over each page to extract text.    
      $allContents = array();
      foreach ($pages as $page) {
          array_push($allContents, $page->getTextArray());   
      }
      $pagenumber = $pagenumber-1;
      if($allContents[$pagenumber]){
        echo json_encode($allContents[$pagenumber], JSON_INVALID_UTF8_IGNORE);
      }
    }      

}

if(isset($_POST['copyUrl'])){
  
  $url = $_SERVER['HTTP_HOST']."/short-app/questionnaires.php?applicationId=".$_REQUEST['applicationId']."&guestId=".rand();
  echo json_encode(array('url' =>$url,'message'=>'success'));
 }


 if(isset($_POST['newUser'])){
   $name = $_POST['mainVal']['name']; 
   $email = $_POST['mainVal']['email']; 
   $mobile = $_POST['mainVal']['mobile']; 
  //  $address = $_POST['mainVal']['address']; 

   $data = array(
    'userId' => $_POST['mainVal']['userid'],    
    'name' => $name,
    'email'=>$email,
    'mobile'=>$mobile,
    'address'=>"",
    'created' => date("Y-m-d H:i:s")
    );
    //Save Newuser data

    $userId = insert('questionnaireUser',$data);
    if($userId){
      $usrdata = array(
        'status' =>200,
        'message'=>'success',
        'userid'=> $userId,
        'name' => $name,
        'email'=> $email,
        'mobile'=> $mobile,
        // 'address'=> $address,
    );
      echo json_encode($usrdata);
    }
 }

// get all New User
if(isset($_POST['getallusr'])){   
    $applicationid = $_POST['appliId'];
 
  $sql = "SELECT *, questionnaireUser.id as newuserid FROM answers LEFT JOIN questionnaireUser on answers.newUserId = questionnaireUser.id WHERE answers.applicationId=".$applicationid." GROUP BY answers.memberId";
  $res = fetch_custom($sql);
  if($res){   
     echo json_encode($res);    
  }else{
    $usrdatares = array(
      'status' =>400,
      'message'=>false,
    );
     echo json_encode($usrdatares);  
  }
}

// get all Application
if(isset($_POST['getallApp'])){ 
  $sql = "select * from newApplications";
  $res = fetch_custom($sql);  
  if($res){   
     echo json_encode($res);    
  }
}


// get all Application
if(isset($_POST['fileExist'])){ 

  $memberId = $_POST['memberId'];
  $applicationId = $_POST['pdfapplicationId'];
  $fileName = $_POST['fileName'];

  $sql = "UPDATE answers SET pdfName = '".$fileName."' WHERE memberId=".$memberId;
  $result = $GLOBALS['conn']->query($sql);
  if($result){
    $res =  array(
      'status' =>200,
      'message'=>true,
      'query'=> $sql
    );
    echo json_encode($res);
  }else{
    $res =  array(
      'status' =>400,
      'message'=>false,
      'query'=> $sql
    );
    echo json_encode($res);
  }
}

// get new Application data
if(isset($_POST['editNewApplication'])){ 
  $newAppliid = $_POST['newAppliid'];

   $sql = "select * from newApplications Where id = $newAppliid";
  $res = fetch_custom($sql);  
  if($res){   
     echo json_encode($res);    
  }
} 

if(isset($_POST['updateNwApplication'])){

  $upAddliId = $_POST['upAddliId'];
  $name = $_POST['name'];
  $FontFamilyE = $_POST['FontFamilyE'];
  $pdfCheckoxE = $_POST['pdfCheckoxE'];
  if($FontFamilyE){
    $FontFamilyE = $FontFamilyE;
  }else{
    $FontFamilyE = '';
  }
  $fontColor = $_POST['fontclr'];
  $fontColor = trim($fontColor);
    if($fontColor){
      $fontColor = str_replace('rgb(', '',$fontColor);
      $fontColor = str_replace(')', '',$fontColor); 
      $fontColor = (explode(",",$fontColor));
      if($fontColor[0] && $fontColor[1] && $fontColor[2] ){
        $fontColor = $fontColor[0].','.$fontColor[1].','.$fontColor[2];
      }else{
        $fontColor = '';
      } 
    }
  $sql = "UPDATE newApplications SET applicationName = '".$name."', fontColor='". $fontColor."', fontFamily='". $FontFamilyE."', isPdfGenerate='". $pdfCheckoxE."'  WHERE id=".$upAddliId;  
  
  $result = $GLOBALS['conn']->query($sql);
  if($result){
    $res =  array(
      'status' =>200,
      'message'=>true,
      'query'=> $sql
    );
    echo json_encode($res);
  }
}

// get Questions answer Application data
if(isset($_POST['ansAppliId'])){ 
  $ansAppliId = $_POST['ansAppliId'];
  $newuserid = $_POST['newuserid'];
  $ansQuesId = $_POST['ansQuesId'];

   $sql = "select * from answers Where questionId ='".$ansQuesId."' AND newUserId='".$newuserid."' AND applicationId='".$ansAppliId."'";
  //  echo $sql; die;
  $res = fetch_custom($sql);  
  if($res){
    array_push($res,['Sql' =>$sql ]);
     echo json_encode($res);    
  }
} 

?>
