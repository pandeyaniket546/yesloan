<?php
define ( 'PROJECT_MODULE', 'Admin' );
define ( 'PROJECT_TITLE', 'Bootstrap PDF Converter' );
define ( 'PROJECT_SHORT_TITLE', 'short-app' );
define ( 'PROJECT_PREFIX', 'short-app' );


define ( 'ERROR_CODE_LOGIN', 'bG9naW4gZXJyb3I=' );
define ( 'ERROR_MSG_LOGIN', 'The email address or password you entered is not valid' );
define ( 'ERROR_CODE_BLOCKED', 'bG9naW4gYmxvY2tlZA==' );
define ( 'ERROR_MSG_BLOCKED', 'Sorry! your account is temporary blocked. Please try after 10 minutes.' );
define ( 'NO_DATA_AVAIL', 'Sorry ! No records found.' );

?>