<?php 
error_reporting(E_ALL);
ini_set('display_errors', '1');
require("database.php");
require("constant.php");  
require("helper.php");

  //Ajax call for save questions for a application
  if(isset($_POST['addQuestion'])){ 
        $textlabel = $_POST['textLable'];
        $textlabel = str_replace("'", "\'", $textlabel);

      $data = array(
          'applicationId'=> $_REQUEST['applicationId'],
          'imageId' => $_POST['imageId'],
          'xCordinates' => $_POST['xCordinate'],
          'yCordinates'=>$_POST['yCordinate'],
          'questionType' => $_POST['questionType'],
          'mappingName' => $_POST['mappingName'],
          'fontColor' => $_POST['fontColor'],
          'fontFamily'=>$_POST['fontFamily'], 
          'questionLable'=>$textlabel,
          'isRequired'=>$_POST['textRequired']=='on' ? 1 : 0  ,
          'isParent'=>$_POST['isParent']=='on' ? 1 : 0 ,
          'parent_id'=>$_POST['parentType']!=''?$_POST['parentType'] : 0,
          'isChild'=>$_POST['ischild']=='on' ? 1 : 0 ,
          'step'=>$_POST['stepWizard']!='' ? $_POST['stepWizard'] : 1 
          );

        // echo '<pre>';  print_r($data); die;
        $save = insert('questions',$data);
        if($save){
          echo json_encode(array("status"=>'question added successfuly'));
        }else{
          echo json_encode(array("status"=>'failed'));
        }
        
  }

  //Ajax call for get all parent question by applicationId
  if(isset($_POST['getParent'])){
    $appIdforparent = $_POST['appIdforparent'];
    if($appIdforparent){
      $sql = "select * from questions where isParent=1 AND applicationId=".$appIdforparent;
      $data = fetch_custom($sql);
      if($data){
        echo json_encode($data);
      } 
    }
        
  }


  //Ajax call for get all question by applicationId
  if(isset($_POST['getQuestion'])){
    $applicationId = $_POST['applicationId'];
    $sql = "select * from questions Where applicationId = $applicationId";
      $data = fetch_custom($sql);
      if($data){
        echo json_encode($data);
      }      

  }

  //Ajax call for getting questions details by questionId and ImageId
  if(isset($_POST['getQuestionByImageId'])){
    $applicationId = $_POST['applicationId'];
    $imageId = $_POST['imageId'];

    $sql = "SELECT * FROM questions WHERE applicationId = $applicationId AND imageId = $imageId";
      $data = fetch_custom($sql);
      if($data){
        echo json_encode($data);
      }      

  }

  //Ajax call for getting result by questionId for pagination

  if(isset($_POST['getQuestionById'])){
    $questionId = $_POST['id'];
    $resultOf = $_POST['resultOf'];
    $questionId = $resultOf=='next' ? $questionId + 1 : $questionId - 1;
    $sql = "select * from questions Where id = $questionId";
      $data = fetch_custom($sql);
      if($data){
        echo json_encode($data);
      }  

  }


  // Get steps of specific application
  if( isset($_POST['getstep']) ){
    $appid = $_POST['getstep'];
    $data1 ="";
    if(isset($_POST['getUsDetails'])){
      $getUsDetails = $_POST['getUsDetails'];
      $sql1 = "SELECT * FROM questionnaireUser where id=".$getUsDetails;
      $data1 = fetch_custom($sql1); 
    }
    $sql = "SELECT MAX(step) as step FROM questions where applicationId=".$appid;
    $data = fetch_custom($sql);
    
      if($data){
        if($data1){
          array_push($data,['userDetails'=>$data1]);
        }
        echo json_encode($data);
      }
  }


 // Get All Questions 
 if( isset($_POST['getappIdData']) ){
  $appid = $_POST['getappIdData'];
  $sql = "select * from questions where applicationId=".$appid;  
    $data = fetch_custom($sql);
    if($data){
       echo json_encode($data);
    } 
 }

  // Get step wise All question
  if(isset($_POST['id'])){   
    $id = ($_POST['id']);
    $steps = ($_POST['steps']);
    $sql = "select * from questions where applicationId=".$id." and step=".$steps;
    $data = fetch_custom($sql);
      if($data){
        echo json_encode($data);
      } 
  }

  // Get all details of perticular Question
  if(isset($_POST['editAppId'])){
    $id = ($_POST['editAppId']); 
    $sql = "select * from questions where applicationId=".$id;
    $data = fetch_custom($sql);
      if($data){
        echo json_encode($data);
      } 
  }

  // get image of perticular appliction Id
  if(isset($_POST['editId'])){
    $id = ($_POST['editId']); 
    // $sql = "select * from questions where id=".$id;
    $sql = "SELECT *,questions.id as editid, applicationDetails.id AS imgId, convertedImages as imgName FROM `questions` LEFT JOIN applicationDetails ON questions.imageId = applicationDetails.id WHERE questions.id=".$id;
    $data = fetch_custom($sql);
      if($data){
        echo json_encode($data);
      } 
  }


// Save Edit question
if(isset($_POST['editQuestion'])){ 
  
  $data = array(
      'questionLable' => $_POST['textLable'],
      'xCordinates' => $_POST['xCordinate'],
      'yCordinates'=>$_POST['yCordinate'],
      'questionType' => $_POST['questionType'],     
      'questionLable'=>$_POST['textLable'],
      'mappingName'=>$_POST['mappingType'],
      'isRequired'=>$_POST['textRequired'] =='on' ? true : false ,
      'isParent'=>$_POST['isParent'] =='on' ? true : false,
//      'parent_id'=>$_POST['parentType']!=''?$_POST['parentType'] : 0,
      'isChild'=>$_POST['ischild'] =='on' ? true : false,
      'step'=>$_POST['stepWizard']!='' ? $_POST['stepWizard'] : 1 
      );
      $editAppId =  $_POST['editAppId'];    

      foreach( $data as $key => $value){        
        $res = update('questions',$key,$value,$editAppId);        
      }     
   
    if($res){
      echo json_encode(array("status"=>"Question Edited Successfully"));
    }else{
      echo json_encode(array("status"=>'failed'));
    }
    
}
// delete Question

if(isset($_POST['delId'])){
  $id = ($_POST['delId']); 
  $sql = "select * from questions where applicationId=".$id;
  $res = delete('questions','id',$id);
  if($res){
    echo json_encode(array("status"=> $res));
  }
}

// get application font and font family
  if(isset($_POST['getfont'])){
    $id = ($_POST['fontappid']); 
    $sql = "select * from newApplications where id=".$id;
    $res = fetch_custom($sql);
    
    if($res){   
      echo json_encode($res);
      
    }
  }

// Save Answer of a perticular application
  if(isset($_POST['ches'])){
        $arr = $_POST['mainVal'];
        $memberid = $arr[0]['value'];
        $applicationId = $arr[1]['value'];
        $userId = $arr[2]['value'];
        // $userId = 2;

        $qname = $arr[3]['value'];
        $qemail = $arr[4]['value'];
        $qmobile = $arr[5]['value'];
        // $qaddress = $arr[6]['value'];
        $qaddress = "";
        $userdaataqu = array();
        if($qname && $qemail && $qmobile ){
          $sql = "UPDATE questionnaireUser SET name = '".$qname."', email='". $qemail."'  , mobile='". $qmobile."', address='". $qaddress."'  WHERE id=".$userId;
          array_push($userdaataqu,$sql);
          $result = $GLOBALS['conn']->query($sql);
         // print_r('user update successfully');
        }
        $emprytest = array();
        $queryresponse = array();
        $date = date("Y-m-d H:i:s");
              /////////////////////////////////
              for ($i=0; $i< count($arr); $i++) { 
          
                $key = ($arr[$i]['name']);
                $value = ($arr[$i]['value']);            
                $key = preg_replace('/[^0-9]/', '',  $key);            
                $answerCheckQuery = "DELETE FROM answers WHERE memberId =  $memberid";
                $answerCheck = delete_custom($answerCheckQuery);
                  
              }
              ///////////////////////////////
        $finalData = [];
       // print_r($arr);die();
        for ($i=0; $i< count($arr); $i++) {  
              $key = ($arr[$i]['name']);
            $value = ($arr[$i]['value']);   
           // print_r($value);         
              $key = preg_replace('/[^0-9]/', '',  $key);    
              $data = fetch_single('questions','questionType','id',$key);
              $dataStep = fetch_single('questions','step','id',$key);
              $dataquestionLable = fetch_single('questions','questionLable','id',$key);
              if($data){
                $quesLab = $dataquestionLable['questionLable'];
                $finalData[$quesLab]  = $value;
                 $quesType = $data['questionType'];
               if($quesType=='signature'){
                for ($j=0; $j< count($arr); $j++) { 
          
                  if($arr[$j]['name']=='signature'){
                     $value = ($arr[$j]['value']); 
                    
                 $cdate = time();
                 $file_name = $cdate.".png";
                 //echo "expected file".$value;
//                 echo "../assest/signatures/".$file_name;
                 $response = base64_to_jpeg($value,"../assest/signatures/".$file_name);
                 $value=$file_name;
                  }

                }
                //echo $response;
               }
              //print_r($value);
              // print_r($quesType);
                $stepNumber = $dataStep['step'];
                $query =  "INSERT INTO answers (applicationId, step, questionType, questionId, answer,memberId,newUserId,createdat) VALUES ('$applicationId','$stepNumber','$quesType', '$key', '$value','$memberid','$userId','$date')"; 
                  array_push($emprytest,$query);
                if ($GLOBALS['conn']->query($query) === TRUE) {
                   // print_r('data inserted'); 
                    $res = $GLOBALS['conn']->insert_id;
                    array_push($queryresponse,$res);
                 }
              }
              
        }
        //die();
       // print_r($arr);
        //die();
    //    echo '<br>';
  //      echo '*************************************';

          $finalData1["application_id"] = $applicationId;
          $finalData1["questionnaire_id"] = $userId;              
          $finalData1["questionnaire_name"] = $qname;
          $finalData1["questionnaire_email"] = $qemail;
          $finalData1["questionnaire_contactno"] = $qmobile;
          $finalData1["step"] = 'Done';
          $finalData1["questionnaires_json"] = $finalData;      

          $arrayFinalData = array(
            'questionnaire_details' => $finalData1,
          );
          $arrayFinalData = array('questionnaire_details' => json_encode($arrayFinalData));
      

       
        $pdfCheckQuery = "select * from newApplications Where id = $applicationId";
                $pdfCheckQuery = fetch_custom($pdfCheckQuery);
                if($pdfCheckQuery){ 
                  //  $pdfCheckQuery = ($pdfCheckQuery[0]['isPdfGenerate']);
                  $pdfCheckQuery = 1;
                }
                
        $data = array(
          'status' =>200,
          'message'=>'success',
          'applicationId'=>$applicationId,
          'pdfCheckQuery' =>$pdfCheckQuery,
      );
      
        echo json_encode($data);
  }

// Save Answer of a perticular Step Wise application
  if(isset($_POST['chesStep'])){
    // echo '<pre>'; print_r($_POST['StepmainVal']); //die('Done');
    //................................................
    if($_POST['StepmainVal']){
      $dataFin = [];
        $mainData = $_POST['StepmainVal'];
         //print_r($mainData);die();
        for($xx = 0; $xx < count($mainData); $xx++){
          // echo '------'.$xx.'------';
          // echo '<br>';
          // echo count($mainData);
          $anser_val =  $mainData[$xx]['anser_val'];
          //print_r($answer_val);die();
          $question_ID =  $mainData[$xx]['question_ID'];
          $member_ID =  $mainData[$xx]['member_ID'];
          $applicationID =  $mainData[$xx]['applicationID'];
          $userID =  $mainData[$xx]['userID'];
          

          $data = fetch_single('questions','questionType','id',$question_ID);
          $dataStep = fetch_single('questions','step','id',$question_ID);
          $quesType = $data['questionType'];
          $stepNumber = $dataStep['step'];
          $date = date("Y-m-d H:i:s");

          $finalData = [];
            $sql = "select * from answers Where applicationId ='".$applicationID."' and  questionId='".$question_ID."' and memberId='".$member_ID."' and newUserId='".$userID."'"; 
         //  echo $sql;
          //  echo '<br>';
           $sql1 = fetch_custom($sql);
           //echo 'sql1111111111111  ';
           //print_r($sql1);

          $sqlQues = "select * from questions Where id ='".$question_ID."'";
          $sqlQues = fetch_custom($sqlQues);
          // echo '<pre>';
          // print_r($sqlQues); 
          // die('Done...22.');
           // $questionLabel = $sqlQues[0]['questionLable'];
            // $dataFin[$questionLabel] = $anser_val;
            
           if($sql1 && $sql1[0]){ 
               //echo 'Update';
              $updateSql = "UPDATE answers SET answer = '".$anser_val."' WHERE id='".$sql1[0]['id']."'";           
              $resultqqqqasd = $GLOBALS['conn']->query($updateSql);

            //  echo '$resultqqqqasd.......'.$resultqqqqasd;
             $sqlSec = "select * from answers Where id ='".$sql1[0]['id']."'";
             $sqlSec = fetch_custom($sqlSec);
              // echo '$sqlSec.......';
              // var_dump($sqlSec);
              if($sqlSec){              
                array_push($finalData,$sqlSec[0]);
              }
           }else{
             //  echo 'Inser wali query ';
               $query =  "INSERT INTO answers (applicationId, step, questionType, questionId, answer,memberId,newUserId,createdat) VALUES ('$applicationID','$stepNumber','$quesType', '$question_ID', '$anser_val','$member_ID','$userID','$date')";
                //echo $query;
                //  echo '<br>';
                if ($GLOBALS['conn']->query($query)) {  
                  // echo 'query ka kya hua ya ha per';
                $res = $GLOBALS['conn']->insert_id;
                  // echo 'What is inser Id.....'.$res;
                  $sqlSec = "select * from answers Where id ='".$res."'";
                  $sqlSec = fetch_custom($sqlSec);
                  // echo 'anser table ....<br>';
                  // var_dump($sqlSec);
                  if($sqlSec && $sqlSec[0]){              
                  array_push($finalData,$sqlSec[0]);
                }
              }
           }

            // echo '$final Data me kya aa raha he';
            // print_r($finalData);              
              
        } // for loop
              $sqlSecDetail = "select * from questionnaireUser Where id ='".$userID."'";
              $sqlSecDetail = fetch_custom($sqlSecDetail);

              
                // "questionnaire_id" => $finalData[0]['questionId'],
              $dataFin["application_id"] = $applicationID;
              $dataFin["questionnaire_id"] = $userID;              
              $dataFin["questionnaire_name"] = $sqlSecDetail[0]['name'];
              $dataFin["questionnaire_email"] = $sqlSecDetail[0]['email'];
              $dataFin["questionnaire_contactno"] = $sqlSecDetail[0]['mobile'];
              $dataFin["step"] = $stepNumber;

              // $questionnaire_details = {
              //   'aa': jsion
              // }
              
             $arrayFinalData = array(
               'questionnaire_details' => $dataFin,
             );
            //  $object = (object)$arrayFinalData;
             $arrayFinalData = array('questionnaire_details' => json_encode($arrayFinalData));

            //  $bb = ($arrayFinalData);
           // echo '<pre>';
           // var_dump($arrayFinalData);
            // die('The End');
            
            
          //**************************************** */
          
            $curl = curl_init();
            // curl_setopt($curl, CURLOPT_PORT, 443);
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://yes.contactprocrm.com/index.php?entryPoint=Questinnaire",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $arrayFinalData,

            // CURLOPT_HTTPHEADER => array(
            //   "Cookie: sugar_user_theme=SuiteP; PHPSESSID=7nfvc3c7jui9kfcufsa3m1gdrk"
            // ),
            CURLOPT_HTTPHEADER => array(
              "cache-control: no-cache",              
            ),
            ));

            // $response = curl_exec($curl);
            // curl_close($curl);          
            // echo $response;
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
              echo "cURL Error #:" . $err;
            } else {                   
             // echo 'Done.....';
              echo $response;

            }



        // } // for loop

      } // $_POST['StepmainVal']
    // die('Done2');
    //................................................  
   }

    // Get child of selec-box 
    if(isset($_POST['parentid'])){
      $parentid = $_POST['parentid'];
      $sql = "select * from questions Where parent_id =  $parentid";
        $data = fetch_custom($sql);
        if($data){
          echo json_encode($data);
        }
    }

  if(isset($_POST['deleteApplication'])){
    $applicationId = $_POST['applicationId'];
    $sql = "select * from newApplications Where id = $applicationId";
    $res = fetch_custom($sql);
    //echo '<pre>'; print_r($res); //die;
    if($res){
       
      
      // echo 'Hello...'; 
      // print_r($status); die;
      
      //.......................................

     // $path = "assest/originalFile/".$res[0]['originalFileName'];
      $path = $_SERVER['HTTP_HOST']."/short-app/assest/originalFile/".$res[0]['originalFileName'];
      // echo $path; 
      // die('parhhhh');
      // $type = pathinfo($path, PATHINFO_EXTENSION);
      // $data_file = file_get_contents($path);
      // // die();
      // $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data_file);

      // echo $base64; die('hellolloo');

      $apiData = array(
        "application_id" => $applicationId,
        "application_name" => $res[0]['applicationName'],
        "file_name" => $res[0]['originalFileName'],
        "created_at" => $date = date("Y-m-d H:i:s"),
        "pdf_access" => $res[0]['isPdfGenerate'],
        "deleted"=>1,
        "file_ext"=>"pdf",
        "file_mime_type"=>"application/pdf",
        "total_pages"=>$res[0]['totalPages'],  
        "pdf_file" => $path            
      );

      $apiCurlData = array('applicatin_details' => $apiData);
      // $apiSendDate = json_encode($apiCurlData);

      $arrayFinalData = array(
        'application_details' => $apiData,
      );
     
      $arrayFinalData = array('application_details' => json_encode($arrayFinalData));
      
      $curl = curl_init();
      // curl_setopt($curl, CURLOPT_PORT, 443);
      curl_setopt_array($curl, array(
      CURLOPT_URL => "https://yes.contactprocrm.com/index.php?entryPoint=ApplicatinList",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS =>$arrayFinalData,
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",  
      ),
    ));

      $response = curl_exec($curl);

      curl_close($curl);
     echo 'delete Application response of Api is...';
     echo $response;

     //die('Api  Working');
     $status = delete_application($applicationId);
      

      //.......................................


      if($status){
      echo json_encode($status);
      }
    }
  }


  function base64_to_jpeg($base64_string, $output_file) {
    //echo $base64_string;
    $ifp = fopen($output_file, "wb"); 
//print_r($ifp);
    $data = explode(',', $base64_string);
//print_r($data);
    fwrite($ifp, base64_decode($data[1])); 
    fclose($ifp); 

    return $output_file; 
}
  //Ajax call for getting labels by page number
  if(isset($_POST['getPageLabel'])){
    $applicationId = $_POST['appId'];
    $imageId = $_POST['imgId'];
    $pagenumber = $_POST['pagenumber']; 

    $sql = "SELECT * FROM newApplications WHERE id = $applicationId";
      $data = fetch_custom($sql);
      if($data){     
          require(__DIR__ . "/../vendor/autoload.php");
        // require(__DIR__ . "/../pdfparser-master/vendor/autoload.php");     
        $originalFileName = $data[0]['originalFileName'];    
        $parser = new \Smalot\PdfParser\Parser();
        $pdf = $parser->parseFile(__DIR__ ."/../assest/originalFile/".$originalFileName);
        $pages  = $pdf->getPages();

        // Loop over each page to extract text.    
        $allContents = array();
        foreach ($pages as $page) {
            array_push($allContents, $page->getTextArray());   
        }
        $pagenumber = $pagenumber-1;
        if($allContents[$pagenumber]){
          echo json_encode($allContents[$pagenumber], JSON_INVALID_UTF8_IGNORE);
        }
      } 
  }

  if(isset($_POST['copyUrl'])){
  
    $url = $_SERVER['HTTP_HOST']."/short-app/questionnaires.php?applicationId=".$_REQUEST['applicationId']."&guestId=".rand();
    echo json_encode(array('url' =>$url,'message'=>'success'));
  }


  if(isset($_POST['newUser'])){
   $name = $_POST['mainVal']['name']; 
   $email = $_POST['mainVal']['email']; 
   $mobile = $_POST['mainVal']['mobile']; 
    //  $address = $_POST['mainVal']['address'];
   $application_ID = $_POST['mainVal']['application_ID'];

    $sql = "select * from questionnaireUser Where name ='".$name."' and  email='".$email."' and mobile='".$mobile."'";          
    $sql1 = fetch_custom($sql);
    $check = true;
    if($sql1 && $sql1[0]){  
      $userrID = $sql1[0]['id'];
      $check = false;
    }

        if($check){
          $data = array(
            'userId' => $_POST['mainVal']['userid'],    
            'name' => $name,
            'email'=>$email,
            'mobile'=>$mobile,
            'address'=>"",
            'applicationId'=>$application_ID,
            'created' => date("Y-m-d H:i:s")
            );
            //Save Newuser data
              $userId = insert('questionnaireUser',$data);
             if($userId){
                $usrdata = array(
                  'status' =>200,
                  'message'=>'success',
                  'userid'=> $userId,
                  'name' => $name,
                  'email'=> $email,
                  'mobile'=> $mobile,
                  'applicationId'=>$application_ID,
                  // 'address'=> $address,
              );
              echo json_encode($usrdata);
              }
           
            
        }else{
          $usrdata = array(
            'status' =>200,
            'message'=>'exist',
            'userid'=> $userrID,
            'name' => $name,
            'email'=> $email,
            'mobile'=> $mobile,
            'applicationId'=>$application_ID,
            // 'address'=> $address,
        );
          echo json_encode($usrdata);
        }
  }

  // get all New User
  if(isset($_POST['getallusr'])){   
      $applicationid = $_POST['appliId'];
  
    $sql = "SELECT *, questionnaireUser.id as newuserid FROM answers LEFT JOIN questionnaireUser on answers.newUserId = questionnaireUser.id WHERE answers.applicationId=".$applicationid."";
    $res = fetch_custom($sql);
    if($res){   
      echo json_encode($res);    
    }else{
      $usrdatares = array(
        'status' =>400,
        'message'=>false,
      );
      echo json_encode($usrdatares);  
    }
  }

  // get all Application
  if(isset($_POST['getallApp'])){ 
    $sql = "select * from newApplications";
    $res = fetch_custom($sql);  
    if($res){   
      echo json_encode($res);    
    }
  }


  // get all Application
  if(isset($_POST['fileExist'])){ 

    $memberId = $_POST['memberId'];
    $applicationId = $_POST['pdfapplicationId'];
    $fileName = $_POST['fileName'];

    $sql = "UPDATE answers SET pdfName = '".$fileName."' WHERE memberId=".$memberId;
    $result = $GLOBALS['conn']->query($sql);
    if($result){
      $res =  array(
        'status' =>200,
        'message'=>true,
        'query'=> $sql
      );
      echo json_encode($res);
    }else{
      $res =  array(
        'status' =>400,
        'message'=>false,
        'query'=> $sql
      );
      echo json_encode($res);
    }
  }

  // get new Application data
  if(isset($_POST['editNewApplication'])){ 
    $newAppliid = $_POST['newAppliid'];

    $sql = "select * from newApplications Where id = $newAppliid";
    $res = fetch_custom($sql);  
    if($res){   
      echo json_encode($res);    
    }
  } 

  if(isset($_POST['updateNwApplication'])){

    $upAddliId = $_POST['upAddliId'];
    $name = $_POST['name'];
    $FontFamilyE = $_POST['FontFamilyE'];
    $pdfCheckoxE = $_POST['pdfCheckoxE'];
    if($FontFamilyE){
      $FontFamilyE = $FontFamilyE;
    }else{
      $FontFamilyE = '';
    }
    $fontColor = $_POST['fontclr'];
    $fontColor = trim($fontColor);
      if($fontColor){
        $fontColor = str_replace('rgb(', '',$fontColor);
        $fontColor = str_replace(')', '',$fontColor); 
        $fontColor = (explode(",",$fontColor));
        if($fontColor[0] && $fontColor[1] && $fontColor[2] ){
          $fontColor = $fontColor[0].','.$fontColor[1].','.$fontColor[2];
        }else{
          $fontColor = '';
        } 
      }
    $sql = "UPDATE newApplications SET applicationName = '".$name."', fontColor='". $fontColor."', fontFamily='". $FontFamilyE."', isPdfGenerate='". $pdfCheckoxE."'  WHERE id=".$upAddliId;  
    
    $result = $GLOBALS['conn']->query($sql);
    if($result){
      $res =  array(
        'status' =>200,
        'message'=>true,
        'query'=> $sql
      );
      echo json_encode($res);
    }
  }

// get Questions answer Application data
  if(isset($_POST['ansAppliId'])){ 
    $ansAppliId = $_POST['ansAppliId'];
    $newuserid = $_POST['newuserid'];
    $ansQuesId = $_POST['ansQuesId'];

    $sql = "select * from answers Where questionId ='".$ansQuesId."' AND newUserId='".$newuserid."' AND applicationId='".$ansAppliId."'";
    //  echo $sql; die;
    $res = fetch_custom($sql);  
    if($res){
      array_push($res,['Sql' =>$sql ]);
      echo json_encode($res);    
    }
  }

  // if(isset($_POST['ansAppliId']) && $_POST['radio'] === 'radio'){ 
  //   $ansAppliId = $_POST['ansAppliId'];
  //   $newuserid = $_POST['newuserid'];
  //   $ansQuesId = $_POST['ansQuesId'];

  //   $sql = "select * from answers Where questionId ='".$ansQuesId."' AND newUserId='".$newuserid."' AND applicationId='".$ansAppliId."'";
  //   //  echo $sql; die;
  //   $res = fetch_custom($sql);  
  //   if($res){
  //     array_push($res,['Sql' =>$sql ]);
  //     echo json_encode($res);    
  //   }
  // }



