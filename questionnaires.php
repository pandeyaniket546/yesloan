<?php 
//phpinfo();

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SHORT-APP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 --> 
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

    <style type="text/css">
form p{margin-top:1em}
#mySignature .radio{
  margin-left:20px;
  display:none
  }.clear-canvas{display:block}
.sign-field{border: 1px solid #428bca;
    padding: 4px;}
.sign{border: 1px silver solid; 
  width: 10}sign-field canvas { 
  borde0%; max-width:500px; height: 200px /* Size of signature image */
}
    </style>
    <link rel="stylesheet" href="bootstrap/css/custom.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="container">
   
 
    <div class="row" style="padding: 7px;">
        <div class="col-md-12">
           <div class="card">
            
   

    <div class="stepwizard" id="stepwizardContainer">
        <div class="stepwizard-row setup-panel">                      
        </div>
    </div>
      <div class="card" style="border: 1px solid #428bca;">
        <div class="submit-questioner">
            <h3 class="card-title pull-left" style="margin-left: 1%;">Submit Questionnaire</h3>
        </div>
        <form role="form" id="subForm" >
          <input type="hidden" value="<?php echo ($_REQUEST['guestId']); ?>" name="memberid" id="memberid"/>
          <input type = "hidden" id="application_ID" name="applicationid" value ="<?php echo $_REQUEST['applicationId']; ?>" />
          <input type="hidden" name="userid" id="userid">

          <input type="hidden" name="qname" id="qname">
          <input type="hidden" name="qemail" id="qemail">
          <input type="hidden" name="qmobile" id="qmobile">
          <!-- <input type="hidden" name="qaddress" id="qaddress"> -->
        </form>
    </div>


    </div>
    <!-- /.login-card-body -->
  </div></div>
<!-- /.login-box -->

<div class="modal" id="modal" tabindex="-1" role="dialog" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">User Details</h3>        
      </div>
      <div class="modal-body">
        <form id="userForm">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Name :<span class="req">* </span></label>
            <input type="text" maxlength="50" minlength="3" class="form-control" required id="name" name="name">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Email :<span class="req">* </span></label>
            <input type="email" class="form-control" required id="email" name="email">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Mobile :<span class="req">* </span></label>
            <input type="text" required minlength="10" maxlength="12" class="form-control" id="mobile" name="mobile">
          </div>          
          <div class="form-group hide">
            <label for="message-text" class="col-form-label">Address : <span class="req">* </span></label>
            <textarea class="form-control" id="address" name="address"></textarea>
          </div>
        
      </div>
      <div class="modal-footer">        
        <button type="submit" class="btn btn-primary" id="adduser">Submit</button>
      </div>
      </form>
    </div>
  </div>
  <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script>

      $(document).ready(function () {
      //called when key is pressed in textbox
        $("#mobile").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
          if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {         
                    return false;
          }
        });
      });


    $("form#userForm").submit(function(e){ 
      e.preventDefault();
     let name = $('#name').val().trim();
     let email = $('#email').val().trim();
     let mobile = $('#mobile').val().trim();
    //  let address = $('#address').val().trim();
     let application_ID = $('#application_ID').val().trim();

     if(name !== '' && email !== ''  && (mobile.length > 9)   ){
      var newUserId = <?php if(isset($_REQUEST['guestId'])){ echo $_REQUEST['guestId']; }?>;
      let datastring = {
        name:name,
        mobile:mobile,
        email:email,
        // address:address,
        userid:newUserId,
        application_ID
      }
      console.log('newUserId...........',newUserId) 
      $.ajax({
          type: "POST",
          url: "config/ajax.php",         
          dataType: "json",
          data: {newUser:"yes", mainVal:datastring},        
          success: function(data) { 
              if(data.message == 'success' && data.status == 200){
                $('#userid').val(data.userid)
                $('form#userForm').trigger("reset");
                $.notify({     
                  title: '<strong> Details submitted successfuly !</strong>',
                  message: '',      
                 },{
                  delay: 3000,
                  z_index:999999999,
                  type: 'success',    
                });
                //-----------------------------------------------------//
                var appid = `<?php echo $_REQUEST['applicationId']; ?>` 
                var guestId = `<?php echo isset($_REQUEST['guestId']); ?>`
                $('#qname').val(data.name)
                $('#qemail').val(data.email)
                $('#qmobile').val(data.mobile)
                // $('#qaddress').val(data.address)
                if(guestId){
                    guestId = 'success';
                }else{
                    guestId = 'Fail'
                }
                getWizardData(appid, guestId,data,0);
                //-----------------------------------------------------//
                $('#modal').hide();
                setTimeout(function(){
                    $('.datepicker').datepicker({
                      autoclose : true,
                      format: 'mm-dd-yyyy',
                    }); 
                }, 2000);

              }
              if(data.message =='exist' && data.status == 200){
                $('#userid').val(data.userid)
                $('form#userForm').trigger("reset");
                $.notify({     
                  title: '<strong> Details submitted successfuly !</strong>',
                  message: '',      
                 },{
                  delay: 3000,
                  z_index:999999999,
                  type: 'success',    
                });

                var appid = `<?php echo $_REQUEST['applicationId']; ?>` 
                var guestId = `<?php echo isset($_REQUEST['guestId']); ?>`
                $('#qname').val(data.name)
                $('#qemail').val(data.email)
                $('#qmobile').val(data.mobile)
                // $('#qaddress').val(data.address)
                if(guestId){
                    guestId = 'success';
                }else{
                    guestId = 'Fail'
                }
                getWizardData(appid, guestId,data,data.userid);
                $('#modal').hide();
                setTimeout(function(){
                    $('.datepicker').datepicker({
                      autoclose : true,
                      format: 'mm-dd-yyyy',
                    }); 
                }, 2000);


              }
            },
          error: function(e) {
              console.log(e);
          }
      });
     }else{
        // alert('Please fill out required field')
        $.notify({     
          title: '<strong>Please fill out required field !</strong>',
          message: '',      
        },{
          delay: 4000,
          z_index:999999999,
          type: 'warning',
        });

     }
   
      
    });
  </script>
</div>



<!-- jQuery -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>

<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/iCheck/icheck.min.js"></script>
<script src="bootstrap/js/custom.js"></script> 
<script src="dist/js/sketch.js"></script>

<script src="dist/js/jquery.signfield-en.min.js"></script>
<script src="dist/js/jquery.signfield.min.js"></script>

<script src="dist/js/pages/wizardValidation.js"></script>
<script src="plugins/bootstrap-notify-master/bootstrap-notify.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="dist/js/custom.js"></script>

<script>

$(document).ready(function(){
    $('#modal').show();//



});

$("form#subForm").submit(function(e){ 
    e.preventDefault();   
    var datastring = $("#subForm").serializeArray();   

    datastring = datastring.concat(
      jQuery('#subForm input[type=checkbox]:not(:checked)').map(
              function() {
                  return {"name": this.name, "value": 1}
      }).get()
    );

    datastring = datastring.concat(
      jQuery('#subForm input[type=radio]:not(:checked)').map(
              function() {
                  return {"name": this.name, "value": 1}
      }).get()
    );
    //console.log('======' +JSON.stringify(datastring));
    var memberId =$('#memberid').val(); 
      console.log('======'+JSON.stringify(datastring));
    $.ajax({
        type: "POST",
        url: "config/ajax.php",         
        dataType: "json",
        data: {ches:"yes", mainVal:datastring},        
        success: function(data) {
             let msg = '';
             if(data.pdfCheckQuery == 1){
               msg = '<strong> Form submitted successfuly and will download in shortly!</strong>'
             }else{
               msg = '<strong> Form submitted successfuly !</strong>'
             }
          if(data.message == "success"){
            $.notify({     
              title: msg,
              message: '',
            },{
              delay: 3000,
              z_index:999999999,
              type: 'success',
            });
          }
         // if(data.pdfCheckQuery == 1){
            window.location.href='generatePdf.php?memberId='+memberId+'&applicationId='+data.applicationId+'&file='+data.pdfCheckQuery;
          //}else{
            //window.location.href='home.php?action=applicationList';
          //}
      
        },
        error: function(e) {
            console.log(e);
        }
    });
  });

  function getval(e){
    let currentvalue = $(e).val();
    let value = $(e).attr('data-value')
    let mappingName = $(e).attr('data-mappingName')
    console.log('value...........', value+'.........'+mappingName)

    if(mappingName.length > 0 ){
      console.log('currentvalue......',currentvalue)
      currentvalue = currentvalue.trim();
      let newmppval = $('#q'+mappingName).val(currentvalue)
      console.log('newmppval,,,,,,,,',newmppval)
    }
  }

  function submitform(){ 
      $('#submitbtn').attr('disabled', true);
      $("#addquesii").trigger('click');
  }

    function getStemData(e){
      let step_value = $(e).attr('data-stepnumber');
      console.log('Next Button Step Value.....',step_value);
      
      let stepData = [];
      $('input,select').each(function(ee) {
        let questStepNum = $(this).attr('data-quesstepnumber');
        if(step_value == questStepNum){
          let answer = $(this).val();
           
           let type = $(this).attr('type');
           if(type === 'radio' || type === 'checkbox'){
            if ($(this).is(":checked")){
              answer = 'on';
            }else{
              answer = 0;
            }
          }
         
            
          let anser_val = answer;
          let data_mainid = $(this).attr('data-mainid');
          let member_ID = $('#memberid').val();
          let applicationID = $('#application_ID').val();
          let userID = $('#userid').val();

          let stepDatatoSend = {
            anser_val : anser_val,
            question_ID: data_mainid,
            member_ID:member_ID,
            applicationID: applicationID,
            userID:userID,
            // userID:2,
          }
          stepData.push(stepDatatoSend)
        }
      });
       //console.log("-----------setdata");
      // ajax call for question save step wise
      $.ajax({
        type: "POST",
        url: "config/ajax.php",         
        dataType: "json",
        data: {chesStep:"yes", StepmainVal:stepData},       
        success: function(data) {
              console.log('answer save response.........',data)
        },
        error: function(e) {
            console.log(e);
        }
      });
    }


</script>



</body>
</html>
