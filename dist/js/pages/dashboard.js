

$(function () {

  "use strict";

  //Make the dashboard widgets sortable Using jquery UI
  $(".connectedSortable").sortable({
    placeholder: "sort-highlight",
    connectWith: ".connectedSortable",
    handle: ".box-header, .nav-tabs",
    forcePlaceholderSize: true,
    zIndex: 999999
  });
  $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

  //jQuery UI sortable for the todo list
  $(".todo-list").sortable({
    placeholder: "sort-highlight",
    handle: ".handle",
    forcePlaceholderSize: true,
    zIndex: 999999
  });

  //bootstrap WYSIHTML5 - text editor
  $(".textarea").wysihtml5();

  $('.daterange').daterangepicker({
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate: moment()
  }, function (start, end) {
    window.alert("You chose: " + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  });

  /* jQueryKnob */
  $(".knob").knob();

  //jvectormap data
  var visitorsData = {
    "US": 398, //USA
    "SA": 400, //Saudi Arabia
    "CA": 1000, //Canada
    "DE": 500, //Germany
    "FR": 760, //France
    "CN": 300, //China
    "AU": 700, //Australia
    "BR": 600, //Brazil
    "IN": 800, //India
    "GB": 320, //Great Britain
    "RU": 3000 //Russia
  };
  //World map by jvectormap
  $('#world-map').vectorMap({
    map: 'world_mill_en',
    backgroundColor: "transparent",
    regionStyle: {
      initial: {
        fill: '#e4e4e4',
        "fill-opacity": 1,
        stroke: 'none',
        "stroke-width": 0,
        "stroke-opacity": 1
      }
    },
    series: {
      regions: [{
        values: visitorsData,
        scale: ["#92c1dc", "#ebf4f9"],
        normalizeFunction: 'polynomial'
      }]
    },
    onRegionLabelShow: function (e, el, code) {
      if (typeof visitorsData[code] != "undefined")
        el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
    }
  });

  //Sparkline charts
  var myvalues = [1000, 1200, 920, 927, 931, 1027, 819, 930, 1021];
  $('#sparkline-1').sparkline(myvalues, {
    type: 'line',
    lineColor: '#92c1dc',
    fillColor: "#ebf4f9",
    height: '50',
    width: '80'
  });
  myvalues = [515, 519, 520, 522, 652, 810, 370, 627, 319, 630, 921];
  $('#sparkline-2').sparkline(myvalues, {
    type: 'line',
    lineColor: '#92c1dc',
    fillColor: "#ebf4f9",
    height: '50',
    width: '80'
  });
  myvalues = [15, 19, 20, 22, 33, 27, 31, 27, 19, 30, 21];
  $('#sparkline-3').sparkline(myvalues, {
    type: 'line',
    lineColor: '#92c1dc',
    fillColor: "#ebf4f9",
    height: '50',
    width: '80'
  });

  //The Calender
  $("#calendar").datepicker();

  //SLIMSCROLL FOR CHAT WIDGET
  $('#chat-box').slimScroll({
    height: '250px'
  });


  

  //Fix for charts under tabs
  $('.box ul.nav a').on('shown.bs.tab', function () {
    area.redraw();
    donut.redraw();
    line.redraw();
  });

  /* The todo list plugin */
  $(".todo-list").todolist({
    onCheck: function (ele) {
      window.console.log("The element has been checked");
      return ele;
    },
    onUncheck: function (ele) {
      window.console.log("The element has been unchecked");
      return ele;
    }
  });

});




$('.applicationImg').on("click",function(){

      $('#addQuestionForm').trigger("reset");
      // $(".select2").select2("val", "");
      // $('.select2').val(null).trigger("change")
      $('#pagelabel').prop("selectedIndex", 0).trigger("change");
      $('#questionType').prop("selectedIndex", 0).trigger("change");
      var imgSrc =$(this).attr('src');
      var pageNumber = $(this).attr('data-pagenumber');      
      var imageId = $(this).attr('id');
      $('#imageId').val(imageId); 
      $('#convertedImg').attr('src',imgSrc);
      getcolorpicket('#fontColor');
      $('#myModal').modal('show');

      var inputJson = {
        getPageLabel : true,
        appId : $('#applicationId').val(),
        imgId : $('#imageId').val(),
        pagenumber : pageNumber
      }

      $.ajax({
        url : "config/ajax.php",
        type: "post", //request type,
        dataType: 'json',
       data: inputJson, //pagelabel
        success: function(response) {        
          $("#pagelabel").children('option:not(:first)').remove();
            $.each(response,function(key, value){
              //console.log('Value.........',value)
              if(((value.trim()).length) > 3){
                let val = value.trim();
                $("#pagelabel").append('<option value="'+ val +'">'+ val+ '</option>');
              } 
            });
        }
      });
})

$('#pagelabel').on("change",function(){
  var pagelabel =$(this).val();
  // console.log('pagelabel......',pagelabel)
  // console.log('pagelabel length......',pagelabel.length)
  if(pagelabel){
    $('#textLable').val(pagelabel);
  }
})

$("#convertedImg").on("click", function(event) {
  
  var parentOffset = $(this).parent().offset();
  var x = Math.round(event.pageX - parentOffset.left);
   var y = Math.round(event.pageY - parentOffset.top);
   // $('#questionModal').modal('show');
  $('#xCordinate').val(x);
  $('#yCordinate').val(y);
  $('#xCordinateInp').val(x);
  $('#yCordinateInp').val(y);
  });

  $("#editImg").on("click", function(event) { //dblclick
    var parentOffset = $(this).parent().offset();   
    var x = Math.round(event.pageX - parentOffset.left);
    var y = Math.round(event.pageY - parentOffset.top); 
    $('#edXCordinateInp').val(x);
    $('#edYCordinateInp').val(y); 
  });


  $('#questionType').on("change",function(){
    var questionType =$(this).val();
   
      // if(questionType.length == 0){
      //   $.notify({     
      //     title: '<strong>Select Any One</strong>',
      //     message: 'Please choose any one option',      
      //   },{
      //     delay: 4000,
      //     z_index:999999999,
      //     type: 'warning',
      //   });
      // }


    if(questionType=='text'){
      $('#addQuestionBtn').attr('disabled',false);
      $('.hideQuestion').attr('display','none');     
      $('#fontSettingsDiv').show();
      $('#mappingDiv').show();
      $('#RadioType').hide();
      $('#isparentDiv').hide();
      $('#ischildDiv').hide();
      $('#selectOption').hide();

    }else if(questionType=='date'){    
      $('#fontSettingsDiv').show();
      $('#RadioType').hide();
      $('#addQuestionBtn').attr('disabled',false);
      $('#isparentDiv').hide();
      $('#ischildDiv').hide();
      $('#selectOption').hide();
      $('#mappingDiv').hide();
    }else if(questionType=='radio'){    
      $('#RadioType').show();
      $('#fontSettingsDiv').show();
      $('#addQuestionBtn').attr('disabled',false);
      $('#isparentDiv').show();
      $('#ischildDiv').show();
      $('#selectOption').hide();
      $('#mappingDiv').hide();
      getParents()
    }else if(questionType=='checkbox'){     
      $('#addQuestionBtn').attr('disabled',false);    
      $('#checkBoxType').show();
      $('#fontSettingsDiv').show();
      $('#isparentDiv').show();
      $('#ischildDiv').show();
      $('#selectOption').hide();
      $('#mappingDiv').hide();
      getParents()
    }else if(questionType=='select'){     
      $('#addQuestionBtn').attr('disabled',false);    
      $('#selectOption').hide();
      $('#checkBoxType').show();
      $('#fontSettingsDiv').show();
      $('#isparentDiv').show();
      $('#ischildDiv').show();
      $('#mappingDiv').hide();
      getParents()
    }else if(questionType=='label'){     
      $('#addQuestionBtn').attr('disabled',false);    
      $('#selectOption').hide();
      $('#checkBoxType').show();
      $('#fontSettingsDiv').show();
      $('#isparentDiv').show();
      $('#ischildDiv').show();
      $('#mappingDiv').hide();
      getParents()
    }else if(questionType=='signature'){ 
      $('#fontSettingsDiv').show();

      $('#addQuestionBtn').attr('disabled',false);    

    }else{
      $('.hideQuestion').attr('display','none');
      $('#addQuestionBtn').attr('disabled',true);      
      $('#RadioType').hide();
      $('#selectOption').hide();
      $('#fontSettingsDiv').hide();
      $('#mappingDiv').hide();
    }
})
  
//append options
$('.addOption').click(function() {
  $('.block:last').before('<div class="form-group"><label>Options</label><input type="text" class="form-control radioOtions optionRemove"  placeholder="Please Enter Options"><button type="button" onclick="removeRow(this)" id="remove_row" class="btn btn-warning pull-right"><span  class="glyphicon glyphicon-remove"></span></button></div>');
});

function removeRow(e){ 
  $(e).parent().remove();
}

function checkvalue(e){
  let value = $(e).val();
  
}

function getParents(){
  let appIdforparent = $('#applicationId').val();
  
  var inputJson = {
    getParent : true,
    appIdforparent:appIdforparent
  }
  if(appIdforparent){
    $.ajax({
      url : "config/ajax.php",
      type: "post", //request type,
      dataType: 'json',
    data: inputJson,
      success: function(response) { 
        $("#parentType").children('option:not(:first)').remove();
          $.each(response,function(key, value){                
            $("#parentType").append('<option value=' + value.id + ' data-step='+value.step+'>' + value.questionLable + '</option>');              
          });
      }
    });
  }
}

function editGetParents(editid, parentId){

  var inputJson = {
    getParent : true,
    appIdforparent:editid

  }
  if(editid){
  $.ajax({
    url : "config/ajax.php",
    type: "post", //request type,
    dataType: 'json',
   data: inputJson,
    success: function(response) { 
      $("#edParentType").children('option:not(:first)').remove();
        $.each(response,function(key, value){                
          $("#edParentType").append('<option value=' + value.id + '>' + value.questionLable + '</option>');              
        });

        $("select#edParentType option").filter(function() {          
          return $(this).val() == parentId;
        }).attr('selected', true);
    }
  });
}
}

function quest_vali(e){

  let que = $(e).val();
  if(que.length > 100){
    $.notify({     
      title: '<strong>Max Length !</strong>',
      message: 'Question Cannot be grater than 100 Character',      
    },{
      delay: 4000,
      z_index:999999999
    });
  }
  if(que.length < 3){
    $.notify({     
      title: '<strong>Please Enter Question Lable </strong>',
      message: ""
    },{
      delay: 4000,
      z_index:999999999,
      type: 'warning',      
    });
  }
}

function step_valid(e){
  let stpes = $(e).val();
  if(stpes.length == 0){
    $.notify({     
      title: '<strong>Step number cannot be empty</strong>',
      message: '',      
    },{
      delay: 4000,
      z_index:999999999,
      type: 'warning',
    });
  }
 }

function parentCheck(e){
  if ($(e).is(":checked")){
    $('#selectOption,#ischildDiv').hide();
    $('#ischild').prop('checked',false);
    $('#parentType').prop('selectedIndex',0);
  }else{
    $('#selectOption').hide();
    $('#ischildDiv').show()
  }
}

function edParentCheck(e){
  if ($(e).is(":checked")){
    $('#selectOption,#ischildDiv').hide();
    $('#edIschild').prop('checked',false);
    $('#edParentType').prop('selectedIndex',0);
  }else{
    $('#selectOption').hide();
    $('#ischildDiv').show()
  }
}

function childCheck(e){
  if ($(e).is(":checked")){
    $('#selectOption').show();
    $('#isparentDiv').hide();
    $('#isParent').prop('checked',false);
  }else{
    $('#isparentDiv').show();
  }
}

function edChildCheck(e){
  if ($(e).is(":checked")){
    $('#selectOption').show();
    $('#isparentDiv').hide();
    $('#edParentType').prop('checked',false);
  }else{
    $('#isparentDiv').show();
  }
}

function isNumber(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
  }
  return true;
}

function check_value(e){
  let val = $(e).val();  
  let step = $(e).find(':selected').attr('data-step');
  if(step){
    if(step.length !== 0 || step !== ''){
      $('#stepWizard').val(step);
      // $('#stepWizard').attr('readonly', true);
    }
  }
  
  if(val.length == 0 || val == ''){
    $.notify({     
      title: '<strong> Select Parent Field !</strong>',
      message: '',      
    },{
      delay: 4000,
      z_index:999999999,
      type: 'warning',
    });
    return false;

  }
}




$('#addQuestionBtn').click(function() {
let xcord = $('#xCordinateInp').val();
let ycord = $('#yCordinateInp').val();
let questionType = $('#questionType').val();
let textLable = $('#textLable').val();
let parentType = $('#parentType').val();
let stepWizard = $('#stepWizard').val();

if((xcord.length) == 0 || (ycord.length) == 0){
  $.notify({     
    title: '<strong>Plese Select Cordinates For Displaying QUestions !</strong>',
    message: '',      
  },{
    delay: 4000,
    z_index:999999999,
    type: 'warning',
  });
  return false;
}
if(questionType.length == 0 || questionType =="" ){
    $.notify({     
      title: '<strong>Plese Select Any Question Type !</strong>',
      message: '',      
    },{
      delay: 4000,
      z_index:999999999,
      type: 'warning',
    });
    return false;
}
if((textLable.length == 0 || textLable =="")){
    $.notify({     
      title: '<strong>Plese Enter Question label Field !</strong>',
      message: '',      
    },{
      delay: 4000,
      z_index:999999999,
      type: 'warning',
    });
    return false;
}

if ($('#ischild').is(":checked")){
    // $('#stepWizard').hide()
    $('#stepWizard').val()
  if(parentType.length == 0 || parentType ==""){
    $.notify({     
      title: '<strong>Plese Select Parent Field !</strong>',
      message: '',      
    },{
      delay: 4000,
      z_index:999999999,
      type: 'warning',
    });
    return false;
  }
}else{
  $('#stepWizard').show()
}
if((stepWizard.length == 0 || stepWizard =="")){
  $.notify({     
    title: '<strong>Plese Enter Step Field !</strong>',
    message: '',      
  },{
    delay: 4000,
    z_index:999999999,
    type: 'warning',
  });
  return false;
}
let mappingName =  $('#mappingType').val();
if(questionType !== 'text'){
    mappingName = 0
}

if(mappingName == ''){
    mappingName = 0
}
 

  var inputJson = {
    addQuestion:"yes",
    textLable : textLable,
    applicationId : $('#applicationId').val(),
    imageId : $('#imageId').val(),
    xCordinate : $('#xCordinate').val(),
    yCordinate : $('#yCordinate').val(),
    questionType : questionType,
    fontColor : $('#fontColor').val(),
    fontFamily : $('#fontFamily').val(),
    stepWizard : stepWizard,
    parentType : parentType,
    mappingName: mappingName
    
  }
  if ($('#textRequired').is(":checked")){
    inputJson.textRequired = 'on';
  }else{
    inputJson.textRequired = 0 ;
  }
  if ($('#isParent').is(":checked")){
    inputJson.isParent = 'on';
  }else{
    inputJson.isParent = 0 ;
  }
  if ($('#ischild').is(":checked")){
    inputJson.ischild = 'on';    
  }else{
    inputJson.ischild = 0 ;
  }
  if($('#ischild').is(":checked") && parentType.length != 0 && (questionType == 'select')){
    inputJson.textRequired = 0 ;
  }


  // console.log('inputJson..........',inputJson);
  // return false
  $.ajax({
    url:"config/ajax.php", //the page containing php script
    type: "post", //request type,
    dataType: 'json',
   data: inputJson,
    success:function(result){
      $('#addQuestionForm').trigger("reset");      
      $('#pagelabel').prop("selectedIndex", 0).trigger("change");
      $('#questionType').prop("selectedIndex", 0).trigger("change");
      $('#mappingType').prop("selectedIndex", 0).trigger("change");
      $('#selectOption,#ischildDiv,#fontSettingsDiv,#isparentDiv').hide();
      $.notify({  
        title: '<strong> Question added successfuly !</strong>',
        message: '',    
      },{
        delay: 4000,
        z_index:999999999,
        type: 'success',
      });
   },
   error: function(result){
    
   }
 });
})

function editQues(id){
  if(id){
    $.ajax({
      url : "config/ajax.php",  
      type: "post", //request type,
      dataType: 'json',
      data: {editId:id},      
      success: function(response) {
        console.log('resp......',response)
        $('select#mappingType').prop('selectedIndex',0)
        editGetParents(response[0].applicationId,response[0].parent_id )
        
          let val = response[0];
          let imgName = response[0].imgName;
          let imgPath = 'assest/convertedImages/'+imgName
          $('#editImg').attr('src',imgPath);

          $('#edTextLable').val(val.questionLable)
          $('#edXCordinateInp').val(val.xCordinates)
          $('#edYCordinateInp').val(val.yCordinates)
          $('#rowId').val(val.editid)   
          
          $("select#edQuestionType option").filter(function() {            
            return $(this).val() == val.questionType;
          }).attr('selected', true);

          if(val.questionType == 'text'){                
            $('#mappingDiv').show();
        }

          $("select#mappingType option").filter(function() {          
            return $(this).val() == val.mappingName;
          }).attr('selected', true);
        
          if((val.mappingName).length > 2){
            $('#edTextRequiredDiv').hide();
            if(val.isRequired == 1){
              ( $('#edTextRequired').prop('checked', true));
            }
          }else{
            if(val.isRequired == 1){
              ( $('#edTextRequired').prop('checked', true));
            }
          }
          if(val.isChild == 1){
            ( $('#edIschild').prop('checked', true));
          }
          if(val.isParent == 1){
            ( $('#edIsParent').prop('checked', true));
          }    
          
          $('#edFontColor').val(val.fontColor);
         
          $("select#edFontFamily option").filter(function() {          
            return $(this).val() == val.fontFamily;
          }).attr('selected', true);
          
          

          if(val.questionType == 'select' && (val.isChild == 1)){
            $('#edStepWizard').val(val.step);            
            // $('#edStepWizard').prop('readonly', true);
            $('#edQuestionType').attr("disabled", true);
          }else{
            $('#edStepWizard').val(val.step);
          }
         
      }
    });
  
    $('#editModal').modal('show');
      getcolorpicket('#edFontColor');
  }
}

function getcolorpicket(e){
  $(e).ColorPicker({
    onSubmit: function(hsb, hex, rgb, el) {
      let rgbColor =  $('.colorpicker_new_color').css("backgroundColor");          
      let rgbNum = getRgbNumbers(rgbColor)         
      $(el).val(rgbNum);          
      $(el).ColorPickerHide();
    },
    onBeforeShow: function () {
      $(this).ColorPickerSetColor(this.value);
    }
  })
}

function getRgbNumbers(color){
  var matchColors = /rgb\((\d{1,3}), (\d{1,3}), (\d{1,3})\)/;
  var match = matchColors.exec(color);
  if (match !== null) {
      return(match[1]+','+ match[2]+','+ match[3]);
  }
}

$('#editQuestionBtn').click(function() {
  let xcord = $('#edXCordinateInp').val();
  let ycord = $('#edYCordinateInp').val();
  let questionType = $('#edQuestionType').val();
  let textLable = $('#edTextLable').val();
//  let parentType = $('#edParentType').val();
  let stepWizard = $('#edStepWizard').val();
  let mappingType = $('#mappingType').val();
  
  if((xcord.length) == 0 || (ycord.length) == 0){
    $.notify({     
      title: '<strong>Please Select Cordinates For Displaying QUestions !</strong>',
      message: '',      
    },{
      delay: 4000,
      z_index:999999999,
      type: 'warning',
    });
    return false;
  }
  if(questionType.length == 0 || questionType =="" ){
      $.notify({     
        title: '<strong>Please Select Any Question Type !</strong>',
        message: '',      
      },{
        delay: 4000,
        z_index:999999999,
        type: 'warning',
      });
      return false;
  }
  if((textLable.length == 0 || textLable =="")){
      $.notify({     
        title: '<strong>Plese Enter Question label Field !</strong>',
        message: '',      
      },{
        delay: 4000,
        z_index:999999999,
        type: 'warning',
      });
      return false;
  }

  
//  if ($('#edIschild').is(":checked")){
//    if(parentType.length == 0 || parentType ==""){
//      $.notify({     
//        title: '<strong>Plese Select Parent Field !</strong>',
//        message: '',      
//       },{
//        delay: 4000,
//        z_index:999999999,
//        type: 'warning',
//      });
//       return false;
//    }
//   }else{
//    parentType = '';
//   }
 
 if((stepWizard.length == 0 || stepWizard =="")){
    $.notify({     
      title: '<strong>Plese Enter Step Field !</strong>',
      message: '',      
    },{
      delay: 4000,
      z_index:999999999,
      type: 'warning',
    });
    return false;
  }
   
  
    var editInputJson = {      
      editQuestion: true,
      textLable : textLable,        
      xCordinate : xcord,
      yCordinate : ycord,
      questionType : questionType,  
      stepWizard : stepWizard,
    //  parentType : parentType,     
      editAppId: $('#rowId').val(),
      mappingType:mappingType
    }

    if ($('#edTextRequired').is(":checked")){
      editInputJson.textRequired = 'on';
    }else{
      editInputJson.textRequired = 0 ;
    }
    if ($('#edIsParent').is(":checked")){
      editInputJson.isParent = 'on';
    }else{
      editInputJson.isParent = 0 ;
     
    }

    if ($('#edIschild').is(":checked")){
      editInputJson.ischild = 'on';    
    }else{
      editInputJson.ischild = 0 ;
    }

//    if($('#edIschild').is(":checked") && parentType.length != 0 && (questionType == 'select')){
      
  //    editInputJson.textRequired = 0 ;
   // }
    console.log('editInputJson.....',editInputJson); 
    $.ajax({
      url:"config/ajax.php", //the page containing php script
      type: "post", //request type,
      dataType: 'json',
      data: editInputJson,
      success:function(result){
        
       $('#editQuestionForm').trigger("reset");
  
      $.notify({     
        title: '<strong> Question edited successfuly !</strong>',
        message: '',      
      },{
        delay: 4000,
        z_index:999999999,
        type: 'success',
      });
      $('#editModal').modal('hide');
     },
     error: function(result){
      
     }
   });
  })

  function delQues(id){
    if(id){
      if (confirm('Are you sure you want to Delete this question ?')) {
        $.ajax({
          url:"config/ajax.php", //the page containing php script
          type: "post", //request type,
          dataType: 'json',
          data: {delId:id},
          success:function(result){
               
          $.notify({
            title: '<strong> Question Deleted successfuly !</strong>',
            message: '',      
          },{
            delay: 2000,
            z_index:999999999,
            type: 'success',
          });
          location.reload();
         },
         error: function(result){
          
         }
       });
      } 

    }
  }

  function delApplication(id){
    if(id){
      if (confirm('Are you sure you want to Delete this question ?')) {
        $.ajax({
          url:"config/ajax.php", //the page containing php script
          type: "post", //request type,
          dataType: 'json',
          data: {deleteApplication :true ,applicationId:id},
          success:function(result){
              //  console.log('Res..1......',result);
          $.notify({
            title: '<strong> Application Deleted Successfuly !</strong>',
            message: '',      
          },{
            delay: 2000,
            z_index:999999999,
            type: 'success',
          });
          location.reload();
         },
         error: function(result){
          
         }
       });
      } 

    }
  }

 
  $("form#subForm").submit(function(e){ 
  //  console.log('sfdfdfdfdfdsfd')
    e.preventDefault();   
    var datastring = $("#subForm").serializeArray();   

    datastring = datastring.concat(
      jQuery('#subForm input[type=checkbox]:not(:checked)').map(
              function() {
                  return {"name": this.name, "value": 'no'}
      }).get()
    );

    datastring = datastring.concat(
      jQuery('#subForm input[type=radio]:not(:checked)').map(
              function() {
                  return {"name": this.name, "value": 'no'}
      }).get()
    );  
   
   //var applicationId =  "<?php echo $_GET['applicationId']; ?>";
    $.ajax({
        type: "POST",
        url: "config/ajax.php",         
        dataType: "json",
        data: {ches:"yes", mainVal:datastring},        
        success: function(data) {               
          if(data.message == "success"){ 
            $.notify({     
              title: '<strong> Form submitted successfuly and will download in shortly!</strong>',
              message: '',      
            },{
              delay: 2000,
              z_index:999999999,
              type: 'success',
            });
          }
         window.location.href='?action=generatePdf&&applicationId='+data.applicationId;
        },
        error: function(e) {
            console.log(e);
        }
    });
  });
  
  function setvalue(e){
   
  }

  function updateQuetionnaireUrl(urlId,applicationId){
    let qUrl=$('#'+urlId).html();
    console.log('qUrl',qUrl);
    $.ajax({
     type: "POST",
     url: "config/ajax.php",         
     dataType: "json",
     data: {copyUrl:"yes", applicationId:applicationId},        
     success: function(data) {       
       if(data.message == "success"){ 
         $.notify({     
           title: '<strong> Questionnaires Url copied!!</strong>',
           message: '',      
         },{
           delay: 2000,
           z_index:999999999,
           type: 'success',
         });
       }
       $('#'+urlId).html(data.url);
     // window.location.href='?action=generatePdf&&applicationId='+data.applicationId;
     },
     error: function(e) {
         console.log(e);
     }
 });
}
 
function getmapping(e){  
  let mapValue = $(e).val();
  if(mapValue){
    $('#txtrequired').hide()    
    $('#textRequired').prop('checked', true);
  }else{
    $('#txtrequired').show();
    $('#textRequired').prop('checked', false);
  }  
}

function edgetmapping(e){  
  let mapValue = $(e).val();
  if(mapValue){
    $('#edTextRequiredDiv').hide()    
    $('#edTextRequired').prop('checked', true);
  }else{
    $('#edTextRequiredDiv').show();
    $('#edTextRequired').prop('checked', false);
  }  
}

function showmapping(e){
   let val = $(e).val();
   if(val == 'text'){
      $('#mappingDiv').show();
      $('#edTextRequiredDiv').hide();
      $('#edTextRequired').prop('checked', true);
      
   }else{
    $('#mappingDiv').hide();
    $('#edTextRequiredDiv').show();
    $('#edTextRequired').prop('checked', false);
    $('select#mappingType').prop('selectedIndex',0)
   }
}

