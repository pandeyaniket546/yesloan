function getData(appid){

    $.ajax({
        url : "config/ajax.php",  
        type: "post", //request type,
        dataType: 'json',
        data: {getstep:appid},      
        success: function(response) {
        // let maxStep = response[0].step;        
        if(response[0].step){
            let maxStep = response[0].step;


            for(let i=0 ; i< maxStep; i++){
                let count = parseInt(i+1);
                let link = '#step-'+count;
                let stepLink = 'step-'+count;

                let saveLink = '#step-'+(maxStep+1)
                let stepSaveLink = 'step-'+(maxStep+1)
               
                let title = 'Step '+count;
                let cls = ''
                
                
                if(count == 1){
                    cls = 'btn btn-primary btn-circle'
                    dis = ''
                }else{
                    cls = 'btn btn-default btn-circle';
                    dis = 'disabled="disabled"'
                }
                //return false
                $(".setup-panel").append('<div class="stepwizard-step"><a href="'+link+'" type="button" class="stop '+cls+'" '+dis+'>'+count+'</a><p>'+title+'</p></div>');
                
                

                if(i == (maxStep-1)){
                     $(".setup-panel").append('<div class="stepwizard-step"><a href="'+saveLink+'" type="button" class="'+cls+'" '+dis+'><span class="round-tab"><span class="fa fa-save"></span></a><p>Save</p></div>');
                 }

                 $("#subForm").append('<div class="row setup-content" id="'+stepLink+'"><div class="col-xs-12"><div class="col-md-8 '+stepLink+'"><button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button></div></div></div>');
               
                if(i == (maxStep-1)){                   
                    $("#subForm").append('<div class="row setup-content" id="'+stepSaveLink+'"><div class="col-xs-12"><div class="col-md-8 "><h2>Are you sure to submit this form ? </h2><button class="btn btn-success btn-lg pull-right" type="submit" id="submitbtn">Submit</button></div></div></div>');
                }
                
                $.ajax({
                    url : "config/ajax.php",  
                    type: "post", //request type,
                    dataType: 'json',
                    data: {id:appid,steps:count},      
                    success: function(response) {
                        // console.log("MY__response.......", response) 
                            $.each(response,function(key, value){
                                
                                let text1 ='';
                                let divId = $('.'+stepLink);

                                if(parseInt(value.isRequired) == 1){
                                    check = 'display:initial';
                                    mstreq = 'mstreq';
                                }else{
                                    check = 'display:none';
                                    mstreq = 'no';
                                }
                                let name = "name"+value.id;
                                if(value.questionType == 'text'){                                    
                                     text1 = '<div class="form-group"><label class="control-label">'+ value.questionLable +'</label><span class="req" style="'+check+'">*</span><input  maxlength="100" type="text" name="'+name+'" class="form-control '+mstreq+'" /></div>'
                                }
                                if(value.questionType == 'checkbox'){
                                      text1 = '<div class="form-group"><input class="form-group '+mstreq+'" name="'+name+'" type="'+ value.questionType +'" value="" onchange="setvalue(this)" id="'+ value.id +'" > <label class="form-check-label" for="'+value.id+'">'+value.questionLable +'</label><span class="req" style="'+check+'">*</span></div>'
                                }
                                if(value.questionType == 'radio'){
                                    text1 = '<div class="form-group"><input class="form-group '+mstreq+'" value="" name="'+name+'" onchange="setvalue(this)" type="'+ value.questionType +'" id="'+ value.id +'"> <label class="form-check-label" for="'+ value.id +'">'+value.questionLable +'</label><span class="req" style="'+check+'">*</span></div>';
                                }
                                if(value.questionType == 'date'){                                    
                                    text1 = '<div class="form-group"><label class="control-label">'+ value.questionLable +'</label><span class="req" style="'+check+'">*</span><input maxlength="100" type="text" name="'+name+'" class="form-control '+mstreq+'" /></div>'
                                }
                                if(value.questionType == 'select' && value.isParent == 1){                                    
                                     text1 = '<div class="form-group"><label class="control-label">'+ value.questionLable +'</label><span class="req" style="'+check+'">*</span><select data-parent="'+value.id+'" id="'+name+'" name="'+name+'" class="form-control '+mstreq+'"><option value="">-- Select --</option></select> <span class="req" style="'+check+'">*</span></div>';                                    
                                    $.ajax({
                                        url : "config/ajax.php",  
                                        type: "post", //request type,
                                        dataType: 'json',
                                        data: {parentid:value.id},
                                        success: function(response) {
                                            console.log('myy data...',response);
                                            $.each(response,function(key, value){
                                              $("#"+name).append('<option value=' + value.questionLable + '>' + value.questionLable + '</option>');
                                            });
                                         }
                                    })
                                
                                }

                                divId.append(text1)
                        })
                    }
                });               
            };  

            // work here

            // var navListItems = $('div.setup-panel div a'),
            // allWells = $('.setup-content'),
            // allNextBtn = $('.nextBtn');
            // allWells.hide();
            // let block = $('#step-1');
            // block.show();
            // block.clod
             ///////////////////////////////////////////////////////////////////////

            var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');
            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                $item = $(this);
                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-primary').addClass('btn-default');
                    $item.addClass('btn-primary');
                    allWells.hide();
                    $target.show();
                   // $target.find('input:eq(0)').focus();

                }
            });

            

            allNextBtn.click(function(){
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"), //step-1 
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"), // step-2                   
                    curInputs = curStep.find("input.mstreq"),
                   
                    isValid = true;

                $(".form-group").removeClass("has-error");
                for(var i=0; i<curInputs.length; i++){
                    
                    if (curInputs[i].value == ""){
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });

               $('div.setup-panel div a.btn-primary').trigger('click');
            
            


                ////////////////////////////////////////////////////////

            

        }

        
        }
    });
};