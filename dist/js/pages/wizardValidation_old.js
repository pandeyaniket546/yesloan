function getWizardData(appid, guestId,usrdata=''){

    $.ajax({
        url : "config/ajax.php",  
        type: "post", //request type,
        dataType: 'json',
        data: {getstep:appid},      
        success: function(response) {   
           
            if(!response[0].step){                
                $('#stepwizardContainer').hide();
                $('#subForm').hide();
                $('.myWarning').show();              
            }
           
            if(response[0].step){
                $('.myWarning').hide()
                let maxStep = response[0].step;

                for(let i=0 ; i< maxStep; i++){
                    let count = parseInt(i+1);
                    let link = '#step-'+count;
                    let stepLink = 'step-'+count;                
                    let title = 'Step '+count;
                    let cls = ''
                    if(i == 0){
                        btnPrevious = '';                   
                    }else{
                        btnPrevious = '<button class="btn btn-primary prevBtn btn" type="button" >Previous</button>';
                    }                   
                    
                    if(count == 1){                
                        cls = 'btn btn-primary btn-circle';
                        dis = 'data-pc';
                    }else{
                        cls = 'btn btn-default btn-circle';
                        dis = 'disabled="disabled"';
                    }
                   
                    $(".setup-panel").append('<div class="stepwizard-step"><a href="'+link+'" type="button" class="'+cls+'" '+dis+'>'+count+'</a><p>'+title+'</p></div>');
                    

                    $("#subForm").append('<div class="row setup-content" id="'+stepLink+'"><div class="col-xs-12"><div class="col-md-8 pad-bot '+stepLink+'"><div class="form-group addDiv">'+btnPrevious+'<button class="btn btn-primary nextBtn btn pull-right" type="button" >Next</button></div></div></div></div>');
                        

                    $.ajax({
                        url : "config/ajax.php",  
                        type: "post", //request type,
                        dataType: 'json',
                        data: {id:appid,steps:count},      
                        success: function(response) {
                            console.log('Data.........',response)          
                            console.log('usrdata.........',usrdata)
                                $.each(response,function(key, value){
                                    let text1 ='';
                                    let divId = $('#'+stepLink+ ' > div > div > div.addDiv');
                                    
                                    if(parseInt(value.isRequired) == 1){
                                        check = 'display:initial';
                                        mstreq = 'mstreq';
                                        requ = 'required="required"';
                                    }else{
                                        check = 'display:none';
                                        mstreq = 'no';
                                        requ = '';
                                    }
                                    let name = "name"+value.id;
                                    let mapvalue = '';
                                    let mappingName = '';
                                    if(value.questionType == 'text'){
                                       
                                        if(usrdata !==""){
                                            mappingName = value.mappingName;
                                            if(mappingName == 'name'){
                                                mapvalue = usrdata.name
                                            }else if(mappingName == 'email'){
                                                mapvalue = usrdata.email
                                            }else if(mappingName == 'mobile'){
                                                mapvalue = usrdata.mobile
                                            }else if(mappingName == 'address'){
                                                mapvalue = usrdata.address
                                            }else{
                                                mapvalue = '';
                                            }
                                        }
                                        text1 = '<div class="form-group"><label class="control-label">'+ value.questionLable +'</label><span class="req" style="'+check+'">*</span><input '+requ+' maxlength="100" onchange="getval(this)" type="text" name="'+name+'" class="form-control '+mstreq+'" value="'+mapvalue+'" data-value="'+mapvalue+'" data-mappingName="'+mappingName+'"/></div>'
                                    }
                                    if(value.questionType == 'date'){                                    
                                        text1 = '<div class="form-group"><label class="control-label">'+ value.questionLable +'</label><span class="req" style="'+check+'">*</span><input '+requ+' maxlength="100" type="text" name="'+name+'" autocomplete=off class="form-control datepicker '+mstreq+'" /></div>'
                                    }
                                    if(value.questionType == 'checkbox'){
                                        text1 = '<div class="form-group"><input class="form-check-input '+mstreq+'" name="'+name+'" '+requ+' type="'+ value.questionType +'" id="'+ value.id +'" > <label class="form-check-label" for="'+value.id+'">'+value.questionLable +'</label><span class="req" style="'+check+'">*</span></div>'
                                    }
                                    if(value.questionType == 'radio'){
                                        text1 = '<div class="form-group"><input class="form-check-input '+mstreq+'" name="'+name+'"  '+requ+' type="'+ value.questionType +'" id="'+ value.id +'"> <label class="form-check-label" for="'+ value.id +'">'+value.questionLable +'</label><span class="req" style="'+check+'">*</span></div>';
                                    }
                                    if(value.questionType == 'label'){
                                        text1 = '<div class=""><label class="form-check-label" for="'+ value.id +'">'+value.questionLable +'</label><span class="req" style="'+check+'">*</span></div>';
                                    }
                                    if(value.questionType == 'select' && value.isParent == 1){                                    
                                        text1 = '<div class="form-group"><label class="control-label">'+ value.questionLable +'</label><span class="req" style="'+check+'">*</span><select data-parent="'+value.id+'" id="'+name+'" '+requ+' name="'+name+'" class="form-control '+mstreq+'"><option value="">-- Select --</option></select></div>';                                    
                                        $.ajax({
                                            url : "config/ajax.php",
                                            type: "post", 
                                            dataType: 'json',
                                            data: {parentid:value.id},
                                            success: function(response) {
                                                
                                                $.each(response,function(key, value){
                                                $("#"+name).append('<option value=' + value.questionLable + '>' + value.questionLable + '</option>');
                                                });
                                            }
                                        })
                                    }
                                    divId.before(text1)
                            })
                        }
                    });               
                };  
            
                    $(".setup-panel").append('<div class="stepwizard-step"><a href="#step-101" type="button" class="btn btn-default btn-circle" disabled="disabled"><span class="round-tab"><span class="fa fa-save"></span></a><p>Save</p></div>'); 
                    if(guestId == 'success'){
                        $("#subForm").append('<div class="row setup-content" id="step-101"><div class="col-xs-12"><div class="col-md-8 pad-bot"><h2>Are you sure to submit this form ?  </h2><button class="btn btn-success btn-lg pull-right" id="submitbtn" onclick="submitform()">Submit</button><button type="submit" id="addquesii"  name="addquesii" class="btn hide">Submit</button></div></div></div>');

                    }else{
                        $("#subForm").append('<div class="row setup-content" id="step-101"><div class="col-xs-12"><div class="col-md-8 pad-bot"><h2>Done </h2><button class="btn btn-success btn-lg pull-right hide" type="submit" id="submitbtn">Submit</button></div></div></div>');
                    }
                   

            // Wizard jquery start

                var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn');
                allPrevBtn = $('.prevBtn');
                allWells.hide();            

                navListItems.click(function (e) {
                    e.preventDefault();
                    var $target = $($(this).attr('href')),
                    $item = $(this);
                    if (!$item.attr("disabled")) {
                        navListItems.removeClass('btn-primary').addClass('btn-default');
                        $item.addClass('btn-primary');
                        allWells.hide();
                        $target.show();
                        $target.find('input:eq(0)').focus();
                    }
                });
                allPrevBtn.click(function(){
                    var curStep = $(this).closest(".setup-content"),
                        curStepBtn = curStep.attr("id"),
                        prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");
                        prevStepWizard.removeAttr('disabled').trigger('click');
                });

                allNextBtn.click(function(){
                    var curStep = $(this).closest(".setup-content"),
                        curStepBtn = curStep.attr("id"),
                        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"), 
                        curInputs = curStep.find("input.mstreq, input.mstreq:checkbox, input.mstreq:radio,select.mstreq"),                   
                        isValid = true;
                    $(".form-group").removeClass("has-error");
                    for(var i=0; i<curInputs.length; i++){
                        if (!curInputs[i].validity.valid){
                            isValid = false;
                            $(curInputs[i]).closest(".form-group").addClass("has-error");
                        }
                }
                    if (isValid)
                        nextStepWizard.removeAttr('disabled').trigger('click');
                });
                $('div.setup-panel div a.btn-primary').trigger('click');
                // Wizard jquery End

            }

        
        }
    });
};


function getWizardDataForAdmin(appid){

    $.ajax({
        url : "config/ajax.php",  
        type: "post", //request type,
        dataType: 'json',
        data: {getstep:appid},      
        success: function(response) {   
           
            if(!response[0].step){                
                $('#stepwizardContainer').hide();
                $('#subFormAdmin').hide();
                $('.myWarning').show();              
            }
           
            if(response[0].step){
                $('.myWarning').hide()
                let maxStep = response[0].step;

                for(let i=0 ; i< maxStep; i++){
                    let count = parseInt(i+1);
                    let link = '#step-'+count;
                    let stepLink = 'step-'+count;                
                    let title = 'Step '+count;
                    let cls = ''
                    if(i == 0){
                        btnPrevious = '';                   
                    }else{
                        btnPrevious = '<button class="btn btn-primary prevBtn btn" type="button" >Previous</button>';
                    }                   
                    
                    if(count == 1){                
                        cls = 'btn btn-primary btn-circle';
                        dis = 'data-pc';
                    }else{
                        cls = 'btn btn-default btn-circle';
                        dis = 'disabled="disabled"';
                    }
                   
                    $(".setup-panel").append('<div class="stepwizard-step"><a href="'+link+'" type="button" class="'+cls+'" '+dis+'>'+count+'</a><p>'+title+'</p></div>');
                    

                    $("#subFormAdmin").append('<div class="row setup-content" id="'+stepLink+'"><div class="col-xs-12"><div class="col-md-8 pad-bot '+stepLink+'"><div class="form-group addDiv">'+btnPrevious+'<button class="btn btn-primary nextBtn btn pull-right" type="button" >Next</button></div></div></div></div>');
                        

                    $.ajax({
                        url : "config/ajax.php",  
                        type: "post", //request type,
                        dataType: 'json',
                        data: {id:appid,steps:count},      
                        success: function(response) {                        
                                $.each(response,function(key, value){
                                    let text1 ='';
                                    let divId = $('#'+stepLink+ ' > div > div > div.addDiv');
                                    
                                    if(parseInt(value.isRequired) == 1){
                                        check = 'display:initial';
                                        mstreq = 'mstreq';
                                        requ = 'required="required"';
                                    }else{
                                        check = 'display:none';
                                        mstreq = 'no';
                                        requ = '';
                                    }
                                    let name = "name"+value.id;
                                    if(value.questionType == 'text'){                                    
                                        text1 = '<div class="form-group"><label class="control-label">'+ value.questionLable +'</label><span class="req" style="'+check+'">*</span><input '+requ+' maxlength="100" type="text" name="'+name+'" class="form-control '+mstreq+'" /></div>'
                                    }
                                    if(value.questionType == 'date'){                                    
                                        text1 = '<div class="form-group"><label class="control-label">'+ value.questionLable +'</label><span class="req" style="'+check+'">*</span><input '+requ+' maxlength="100" type="text" name="'+name+'" autocomplete=off class="form-control datepicker '+mstreq+'" /></div>'
                                    }
                                    if(value.questionType == 'checkbox'){
                                        text1 = '<div class="form-group"><input class="form-check-input '+mstreq+'" name="'+name+'" '+requ+' type="'+ value.questionType +'" id="'+ value.id +'" > <label class="form-check-label" for="'+value.id+'">'+value.questionLable +'</label><span class="req" style="'+check+'">*</span></div>'
                                    }
                                    if(value.questionType == 'radio'){
                                        text1 = '<div class="form-group"><input class="form-check-input '+mstreq+'" name="'+name+'"  '+requ+' type="'+ value.questionType +'" id="'+ value.id +'"> <label class="form-check-label" for="'+ value.id +'">'+value.questionLable +'</label><span class="req" style="'+check+'">*</span></div>';
                                    }
                                    if(value.questionType == 'select' && value.isParent == 1){                                    
                                        text1 = '<div class="form-group"><label class="control-label">'+ value.questionLable +'</label><span class="req" style="'+check+'">*</span><select data-parent="'+value.id+'" id="'+name+'" '+requ+' name="'+name+'" class="form-control '+mstreq+'"><option value="">-- Select --</option></select></div>';                                    
                                        $.ajax({
                                            url : "config/ajax.php",
                                            type: "post", 
                                            dataType: 'json',
                                            data: {parentid:value.id},
                                            success: function(response) {
                                                
                                                $.each(response,function(key, value){
                                                $("#"+name).append('<option value=' + value.questionLable + '>' + value.questionLable + '</option>');
                                                });
                                            }
                                        })
                                    }
                                    divId.before(text1)
                            })
                        }
                    });               
                };  
            
                    $(".setup-panel").append('<div class="stepwizard-step"><a href="#step-101" type="button" class="btn btn-default btn-circle" disabled="disabled"><span class="round-tab"><span class="fa fa-save"></span></a><p>Save</p></div>');
                            
                    $("#subFormAdmin").append('<div class="row setup-content" id="step-101"><div class="col-xs-12"><div class="col-md-8 "><h2>Are you sure to submit this form ? </h2><button class="btn btn-success btn-lg pull-right" type="button">Submit</button></div></div></div>');

            // Wizard jquery start

                var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn');
                allPrevBtn = $('.prevBtn');
                allWells.hide();            

                navListItems.click(function (e) {
                    e.preventDefault();
                    var $target = $($(this).attr('href')),
                    $item = $(this);
                    if (!$item.attr("disabled")) {
                        navListItems.removeClass('btn-primary').addClass('btn-default');
                        $item.addClass('btn-primary');
                        allWells.hide();
                        $target.show();
                        $target.find('input:eq(0)').focus();
                    }
                });
                allPrevBtn.click(function(){
                    var curStep = $(this).closest(".setup-content"),
                        curStepBtn = curStep.attr("id"),
                        prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");
                        prevStepWizard.removeAttr('disabled').trigger('click');
                });

                allNextBtn.click(function(){
                    var curStep = $(this).closest(".setup-content"),
                        curStepBtn = curStep.attr("id"),
                        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"), 
                        curInputs = curStep.find("input.mstreq, input.mstreq:checkbox, input.mstreq:radio,select.mstreq"),                   
                        isValid = true;
                    $(".form-group").removeClass("has-error");
                    for(var i=0; i<curInputs.length; i++){
                        if (!curInputs[i].validity.valid){
                            isValid = false;
                            $(curInputs[i]).closest(".form-group").addClass("has-error");
                        }
                }
                    if (isValid)
                        nextStepWizard.removeAttr('disabled').trigger('click');
                });
                $('div.setup-panel div a.btn-primary').trigger('click');
                // Wizard jquery End

            }

        
        }
    });
};



function editQuestion(appid){
    $.ajax({
        url: "config/ajax.php",
        type: "post", //request type,
        dataType: 'json',
        data: {editAppId: appid},
        success: function(response) {
         console.log('Response....ed..',response)
            $.each(response, function(key, value) {
                let count = key + 1;
                let mapname = value.mappingName;
                if(mapname.length > 2){
                    mapname = mapname.toUpperCase()
                }else{
                    mapname = 'Not Mapped';
                }

                $('#edques').append('<tr><td>' + count + '</td><td>' + value.questionLable +
                    '</td><td>' + (value.questionType).toUpperCase() +
                    '</td><td>'+mapname+'</td><td><a onclick="editQues(' + value.id +')" class="btn btn-primary">Edit</a></td><td><a onclick="delQues(' + value.id +')" class="btn btn-warning">Delete</a></td></tr>')
            })
            $('#table_id').DataTable({
                "name": "edit", "orderable": "false"
            });

        }
    });

}


function getAllApplication(){
    $.ajax({
        url: "config/ajax.php",
        type: "post", //request type,
        dataType: 'json',
        data: {getallApp: true},
        success: function(response) {
         console.log('Response.........',response);
         $.each(response,function(key, value){
            $("#SearcApp").append('<option value=' + value.id + '>' + value.applicationName + '</option>');
            });
        }
    });

}

function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}



